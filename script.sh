#!/bin/bash

# Check if root #
if (( "$EUID" != 0 ))
  then 	echo "Please run as root";
	exit 1;
fi

# Update and upgrade the vm
sudo apt update
sudo apt upgrade

# Add apt repository
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://mirrors.accretive-networks.net/mariadb/repo/10.3/ubuntu bionic main'

# Install mariadb

sudo apt install mariadb-server

# Start mariadb in safe mode to change root authentication method

sudo /etc/init.d/mysql stop
sudo mysqld_safe --skip-grant-tables &

# Set root authentication method

echo "
In another terminal enter the following commands :

mysql -u root
USE mysql;
UPDATE user SET authentication_string=PASSWORD('user42') WHERE User='root';
UPDATE user SET plugin='mysql_native_password' WHERE User='root';
FLUSH privileges;
CREATE DATABASE hypertubedb;
CREATE USER IF NOT EXISTS mate@localhost IDENTIFIED BY 'matcha';
GRANT ALL PRIVILEGES ON hypertubedb.* TO mate@localhost;
quit;
"
read -p "
Then press ENTER:
"
# Install maven
sudo apt install maven

# Install JDK 15
wget https://download.java.net/java/GA/jdk15.0.2/0d1cfde4252546c6931946de8db48ee2/7/GPL/openjdk-15.0.2_linux-x64_bin.tar.gz
sudo tar -xf openjdk-15.0.2_linux-x64_bin.tar.gz
sudo cp -R jdk-15.0.2 /usr/lib/jvm/.

# Set alternative JAVA version to jdk15
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk-15.0.2/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk-15.0.2/bin/javac" 0
sudo update-alternatives --install "/usr/bin/javap" "javap" "/usr/lib/jvm/jdk-15.0.2/bin/javap" 0
sudo update-alternatives --set java /usr/lib/jvm/jdk-15.0.2/bin/java
sudo update-alternatives --set javap /usr/lib/jvm/jdk-15.0.2/bin/javap

# Instructions to run all three servers
echo "
You will now run the servers. In order to do so, open 3 new terminals:

In the first one, run from the frontend directory of the project:
npm install
npm start

In the second one, run from the nodejs directory of the project:
npm install
node app.js

Finally in the third terminal, run from backend directory:
mvn compile
mvn install
mvn exec:java -Dexec.mainClass=org.hypertube.App

Last but not least, create the database by navigating to http://localhost:4567/initDatabase

You can now use the service ! Navigate to http://localhost:4200
"
