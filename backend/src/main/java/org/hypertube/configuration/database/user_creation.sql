CREATE USER IF NOT EXISTS watcher@localhost IDENTIFIED BY 'user42';
GRANT ALL PRIVILEGES ON hypertubedb.* TO watcher@localhost;
