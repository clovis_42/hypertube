package org.hypertube.configuration.database;

import java.io.*;
import java.sql.*;

import org.apache.ibatis.jdbc.ScriptRunner;

public class DatabaseManagement {
    private static final String MARIA_HOST = "jdbc:mariadb://localhost:3306/?useSSL=false";
    private static final String path = new File("").getAbsolutePath() + "/src/main/java/org/hypertube/configuration/database/";
    private static final String USER_CREATION_SQL = "user_creation.sql";
    private static final String TABLES_CREATION_SQL = "tables_creation.sql";
    private static final String DATABASE_CREATION_SQL = "database_creation.sql";
    private static final String ROOT_USERNAME = "root";
    private static final String PASSWORD = "user42";
    private static final String WATCHER = "watcher";
    private static final String HYPERTUBE_URL = "jdbc:mariadb://localhost:3306/hypertubedb";
    private static ScriptRunner rootSr;

    public DatabaseManagement() throws SQLException {
        Connection rootConnection = DriverManager.getConnection(MARIA_HOST, ROOT_USERNAME, PASSWORD);
        rootSr = new ScriptRunner(rootConnection);
    }

    public void createDatabase() throws FileNotFoundException {
        Reader DBCreateScript = getScript(DATABASE_CREATION_SQL);
        rootSr.runScript(DBCreateScript);
    }

    public void createUser() throws FileNotFoundException {
        Reader createUserScript = getScript(USER_CREATION_SQL);
        rootSr.runScript(createUserScript);
    }

    public void createTables() throws FileNotFoundException, SQLException {
        Connection userConnection = DriverManager.getConnection(HYPERTUBE_URL, WATCHER, PASSWORD);
        ScriptRunner userSr = new ScriptRunner(userConnection);

        Reader createTablesScript = getScript(TABLES_CREATION_SQL);
        userSr.runScript(createTablesScript);
    }

    private BufferedReader getScript(String databaseCreation) throws FileNotFoundException {
        return new BufferedReader(new FileReader(path + databaseCreation));
    }
}
