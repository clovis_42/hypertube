CREATE TABLE USERS(
    userId Int AUTO_INCREMENT NOT NULL UNIQUE PRIMARY KEY,
    firstname Varchar (50),
    lastname Varchar (50),
    username Varchar (50) NOT NULL UNIQUE,
    password Varchar (512),
    email Varchar (50) UNIQUE,
    authToken Varchar (255),
    emailToken Varchar (255),
    picture MEDIUMBLOB NOT NULL,
    confirmed Int DEFAULT 0,
    ftId int DEFAULT NULL,
    gitHubId int DEFAULT NULL
);

CREATE TABLE MOVIES_WATCHED(
    movieId Varchar(11) NOT NULL PRIMARY KEY,
    userId Int NOT NULL
);

CREATE TABLE COMMENTS(
    commentId Int(11) NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
    movieId Varchar(11) NOT NULL,
    userId Int NOT NULL,
    content Varchar(241) NOT NULL,
    creationDate DATETIME DEFAULT CURRENT_TIMESTAMP
);
