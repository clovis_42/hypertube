package org.hypertube.entity;

import lombok.Data;

@Data
public class Email {
    String username;
    String recipient;
    String object;
    String content;

    public Email(String username, String recipient, String object, String content) {
        this.setUsername(username);
        this.setRecipient(recipient);
        this.setObject(object);
        this.setContent(content);
    }
}
