package org.hypertube.entity;

import lombok.Data;

import java.util.ArrayList;

@Data
public class User {
    private int userId;
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String confirmedPassword;
    private String email;
    private String authToken;
    private String emailToken;
    private String picture;
    private boolean confirmed;
    private int ftId;
    private int ghId;
    private ArrayList<String> watchedMovies;

    public User(String username, String firstname, String lastname, String picture, String email, int ftId) {
        this.setUsername(username);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setEmail(email);
        this.setPicture(picture);
        this.setFtId(ftId);
        this.setConfirmed(true);
    }

    public User(String username, String picture, int ghId) {
        this.setUsername(username);
        this.setPicture(picture);
        this.setGhId(ghId);
        this.setConfirmed(false);
    }

    public User() {
    }
}
