package org.hypertube.entity;

import lombok.Data;

import java.sql.Date;

@Data
public class Comment {
    private String movieId;
    private int userId;
    private String username;
    private Date createdAt;
    private String content;

    public Comment(String movieId, int userId, String username, Date createdAt, String content) {
        this.setMovieId(movieId);
        this.setUserId(userId);
        this.setUsername(username);
        this.setCreatedAt(createdAt);
        this.setContent(content);
    }
    public Comment(){}
}
