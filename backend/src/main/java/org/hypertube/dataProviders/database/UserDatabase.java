package org.hypertube.dataProviders.database;

import org.hypertube.entity.User;

import java.sql.*;
import java.util.ArrayList;

public class UserDatabase {

    Connection userConnection;
    private static final String HYPERTUBE_URL = "jdbc:mariadb://localhost:3306/hypertubedb";
    private static final String WATCHER = "watcher";
    private static final String PASSWORD = "user42";

    public UserDatabase() throws SQLException {
        userConnection = DriverManager.getConnection(HYPERTUBE_URL, WATCHER, PASSWORD);
    }

    public boolean checkLogins(String username, String hashedPassword) throws SQLException {
        String query = "SELECT COUNT(*) FROM USERS WHERE username = ? AND password = ? AND confirmed = 1";
        PreparedStatement selectStatement = userConnection.prepareStatement(query);
        selectStatement.setString(1, username);
        selectStatement.setString(2, hashedPassword);
        ResultSet selectResult = selectStatement.executeQuery();
        selectResult.first();
        return selectResult.getInt(1) != 0;
    }

    public User getUserData(String username) throws SQLException {
        User userData = new User();
        String query = "SELECT userId, username, firstname, lastname, email, picture, confirmed, gitHubId, ftId "
                + "FROM USERS"
                + " WHERE username = ?";
        PreparedStatement getDataStatement = userConnection.prepareStatement(query);
        getDataStatement.setString(1, username);
        ResultSet result = getDataStatement.executeQuery();
        if (result.first()) {
            userData.setUserId(result.getInt("userId"));
            userData.setUsername(result.getString("username"));
            userData.setFirstname(result.getString("firstname"));
            userData.setLastname(result.getString("lastname"));
            userData.setEmail(result.getString("email"));
            userData.setPicture(result.getString("picture"));
            userData.setConfirmed(result.getInt("confirmed") == 1);
            userData.setGhId(result.getInt("gitHubId"));
            userData.setFtId(result.getInt("ftId"));
        }
        return userData;
    }

    public ArrayList<String> getWatchedMoviesList(User user) throws SQLException {
        String query = "SELECT movieId FROM MOVIES_WATCHED WHERE userId = ?";
        PreparedStatement queryStatement = userConnection.prepareStatement(query);
        queryStatement.setInt(1, user.getUserId());
        ResultSet result = queryStatement.executeQuery();
        result.beforeFirst();
        ArrayList<String> moviesList = new ArrayList<>();
        while (result.next()) {
            moviesList.add(result.getString("movieId"));
        }
        return moviesList;
    }

    public void updateToken(String username, String encodedToken) throws SQLException {
        String updateString = "UPDATE USERS SET authToken = ? WHERE username = ?";
        PreparedStatement updateStatement = userConnection.prepareStatement(updateString);
        updateStatement.setString(1, encodedToken);
        updateStatement.setString(2, username);
        updateStatement.executeUpdate();
    }

    public String getUsername(String column, int value) throws SQLException {
        String query = "SELECT username FROM USERS WHERE " + column + "=?";
        PreparedStatement queryStatement = userConnection.prepareStatement(query);
        queryStatement.setInt(1, value);
        ResultSet resultQuery = queryStatement.executeQuery();
        resultQuery.first();
        return resultQuery.getString("username");
    }

    public boolean userExists(String column, String value) throws SQLException {
        String query = "SELECT COUNT(*) FROM USERS WHERE " + column + "= ?";
        PreparedStatement countStatement = userConnection.prepareStatement(query);
        countStatement.setString(1, value);
        ResultSet countResult = countStatement.executeQuery();
        countResult.first();
        return countResult.getInt(1) != 0;
    }

    public void createUser(User user, String passwordHash, String emailTokenHash) throws SQLException {
        String query =
                "INSERT INTO USERS (firstname, lastname, username, email, picture, password, emailToken) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement insertionStatement = userConnection.prepareStatement(query);
        insertionStatement.setString(1, user.getFirstname());
        insertionStatement.setString(2, user.getLastname());
        insertionStatement.setString(3, user.getUsername());
        insertionStatement.setString(4, user.getEmail());
        insertionStatement.setString(5, user.getPicture());
        insertionStatement.setString(6, passwordHash);
        insertionStatement.setString(7, emailTokenHash);
        if (insertionStatement.executeUpdate() != 1) {
            throw new SQLException();
        }
    }

    public boolean checkUserToken(String username, String column, String hashedToken) throws SQLException {
        String query = "SELECT COUNT(*) FROM USERS WHERE username = ? AND " + column + " = ?";
        PreparedStatement countStatement = userConnection.prepareStatement(query);
        countStatement.setString(1, username);
        countStatement.setString(2, hashedToken);
        ResultSet countResult = countStatement.executeQuery();
        countResult.first();
        return countResult.getInt(1) != 0;
    }

    public void confirmAccount(String username, int status) throws SQLException {
        String updateQuery = "UPDATE USERS SET confirmed = ? WHERE username = ?";
        PreparedStatement updateStatement = userConnection.prepareStatement(updateQuery);
        updateStatement.setInt(1, status);
        updateStatement.setString(2, username);
        updateStatement.executeUpdate();
    }

    public String getValueBy(String toGet, String column, String value) throws SQLException {
        String query = "SELECT " + toGet + " FROM USERS WHERE " + column + " = ?";
        PreparedStatement statement = userConnection.prepareStatement(query);
        statement.setString(1, value);
        ResultSet result = statement.executeQuery();
        if (result.first()) {
            return (result.getString(toGet));
        }
        return null;
    }

    public void updateEmailToken(String token, String email) throws SQLException {
        String query = "UPDATE USERS SET emailToken = ? WHERE email = ?";
        PreparedStatement updateStatement = userConnection.prepareStatement(query);
        updateStatement.setString(1, token);
        updateStatement.setString(2, email);
        updateStatement.executeUpdate();
    }

    public void updatePassword(String username, String hashedPassword) throws SQLException {
        String query = "UPDATE USERS SET password = ? WHERE username = ?";
        PreparedStatement updateStatement = userConnection.prepareStatement(query);
        updateStatement.setString(1, hashedPassword);
        updateStatement.setString(2, username);
        updateStatement.executeUpdate();
    }

    public void insertFtUser(User user) throws SQLException {
        String query =
                "INSERT INTO USERS (firstname, lastname, username, email, picture, ftId, confirmed)" +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement insertionStatement = userConnection.prepareStatement(query);
        insertionStatement.setString(1, user.getFirstname());
        insertionStatement.setString(2, user.getLastname());
        insertionStatement.setString(3, user.getUsername());
        insertionStatement.setString(4, user.getEmail());
        insertionStatement.setString(5, user.getPicture());
        insertionStatement.setInt(6, user.getFtId());
        insertionStatement.setInt(7, user.isConfirmed() ? 1 : 0);
        if (insertionStatement.executeUpdate() != 1) {
            throw new SQLException();
        }
    }

    public void insertGitHubUser(User user) throws SQLException {
        String query =
                "INSERT INTO USERS (username, picture, gitHubId, confirmed)" +
                        " VALUES (?, ?, ?, ?)";
        PreparedStatement insertionStatement = userConnection.prepareStatement(query);
        insertionStatement.setString(1, user.getUsername());
        insertionStatement.setString(2, user.getPicture());
        insertionStatement.setInt(3, user.getGhId());
        insertionStatement.setInt(4, user.isConfirmed() ? 1 : 0);
        if (insertionStatement.executeUpdate() != 1) {
            throw new SQLException();
        }
    }

    public void updateSettings(User user) throws SQLException {
        String updateQuery = "UPDATE USERS SET firstname = ?, lastname = ?, password = ?, email = ?, picture = ? "
                + "WHERE username = ?";
        PreparedStatement statement = userConnection.prepareStatement(updateQuery);
        statement.setString(1, user.getFirstname());
        statement.setString(2, user.getLastname());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getPicture());
        statement.setString(6, user.getUsername());
        statement.executeUpdate();
    }

    public boolean userIsConfirmed(String column, String value) throws SQLException {
        String query = "SELECT COUNT(*) FROM USERS WHERE confirmed = 1 AND " + column + " = ?";
        PreparedStatement countStatement = userConnection.prepareStatement(query);
        countStatement.setString(1, value);
        ResultSet countResult = countStatement.executeQuery();
        countResult.first();
        return countResult.getInt(1) == 1;
    }

    public void updateGitHubProfile(User user, String emailToken) throws SQLException {
        String updateQuery = "UPDATE USERS SET firstname = ?, lastname = ?, email = ?, emailToken = ? WHERE gitHubId =  ?";
        PreparedStatement statement = userConnection.prepareStatement(updateQuery);
        statement.setString(1, user.getFirstname());
        statement.setString(2, user.getLastname());
        statement.setString(3, user.getEmail());
        statement.setString(4, emailToken);
        statement.setInt(5, user.getGhId());
        statement.executeUpdate();
    }

    public User getProfileData(int profileId) throws SQLException {
        User userData = new User();
        String query = "SELECT username, firstname, lastname, picture"
                + " FROM USERS"
                + " WHERE userId = ?";
        PreparedStatement getDataStatement = userConnection.prepareStatement(query);
        getDataStatement.setInt(1, profileId);
        ResultSet result = getDataStatement.executeQuery();
        if (result.first()) {
            userData.setUsername(result.getString("username"));
            userData.setFirstname(result.getString("firstname"));
            userData.setLastname(result.getString("lastname"));
            userData.setPicture(result.getString("picture"));
        }
        return userData;
    }

    public void setMovieWatched(String userId, String movieId) throws SQLException {
        String query =
                "INSERT INTO MOVIES_WATCHED (movieId, userId)" +
                        " VALUES (?, ?)";
        PreparedStatement insertionStatement = userConnection.prepareStatement(query);
        insertionStatement.setString(1, movieId);
        insertionStatement.setString(2, userId);
        if (insertionStatement.executeUpdate() != 1) {
            throw new SQLException();
        }
    }
}
