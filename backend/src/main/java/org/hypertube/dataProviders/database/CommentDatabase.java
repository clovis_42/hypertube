package org.hypertube.dataProviders.database;

import org.hypertube.entity.Comment;

import java.sql.*;
import java.util.ArrayList;

public class CommentDatabase {
    Connection userConnection;
    private static final String HYPERTUBE_URL = "jdbc:mariadb://localhost:3306/hypertubedb";
    private static final String WATCHER = "watcher";
    private static final String PASSWORD = "user42";

    public CommentDatabase() throws SQLException {
        userConnection = DriverManager.getConnection(HYPERTUBE_URL, WATCHER, PASSWORD);
    }

    public void insertComment(Comment comment) throws SQLException {
        String query = "INSERT INTO COMMENTS (movieId, userId, content) " +
                "VALUES (?, ?, ?)";
        PreparedStatement insertionStatement = userConnection.prepareStatement(query);
        insertionStatement.setString(1, comment.getMovieId());
        insertionStatement.setInt(2, comment.getUserId());
        insertionStatement.setString(3, comment.getContent());
        if (insertionStatement.executeUpdate() != 1) {
            throw new SQLException();
        }
    }

    public ArrayList<Comment> getCommentsBy(String column, String value) throws SQLException {
        String query = "SELECT movieId, commentTable.userId, creationDate, content, username " +
                "FROM COMMENTS AS commentTable " +
                "JOIN USERS AS userTable " +
                "ON commentTable.userId = userTable.userId " +
                "WHERE " + column + "= ?";
        PreparedStatement getCommentsStatement = userConnection.prepareStatement(query);
        getCommentsStatement.setString(1, value);
        ResultSet result = getCommentsStatement.executeQuery();
        ArrayList<Comment> comments = new ArrayList<>();
        result.beforeFirst();
        while (result.next()) {
            Comment comment = new Comment(
                    result.getString("movieId"),
                    result.getInt("userId"),
                    result.getString("username"),
                    result.getDate("creationDate"),
                    result.getString("content")
            );
            comments.add(comment);
        }
        return comments;
    }
}
