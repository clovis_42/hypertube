package org.hypertube;

import org.hypertube.endpoints.CommentEndpoint;
import org.hypertube.endpoints.CorsRequestEndpoint;
import org.hypertube.endpoints.DatabaseManagementEndPoint;
import org.hypertube.endpoints.UserEndpoint;

import static org.hypertube.endpoints.AsyncValidatorsEndpoint.emailExists;
import static org.hypertube.endpoints.AsyncValidatorsEndpoint.usernameExists;
import static spark.Spark.*;

public class App {
    public static void main(String[] args) {
        options("/*", (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Request-Method", "*");
            response.header("Access-Control-Allow-Headers", "*");
            request.headers();

        });

        after((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "*");
        });

        get("/initDatabase", DatabaseManagementEndPoint.initDB);
        get("/corsRequest", CorsRequestEndpoint.getCorsRequest);
        path("/validators", () -> {
            get("/usernameExists", usernameExists);
            get("/emailExists", emailExists);
        });
        path("/comment", () -> {
            get("/get", CommentEndpoint.getComments);
            post("/post", CommentEndpoint.createComment);
        });
        path("/user", () -> {
            post("/authenticate", UserEndpoint.signIn);
            post("/createUser", UserEndpoint.createUser);
            post("/confirmAccount", UserEndpoint.confirmAccount);
            post("/movieWatched", UserEndpoint.setMovieWatched);
            path("/password", () -> {
                post("/postReset", UserEndpoint.resetPassword);
                post("/postUpdate", UserEndpoint.updatePassword);
            });
            post("/update", UserEndpoint.updateSettings);
            path("/oauth", () -> {
                post("/connection", UserEndpoint.oauthConnection);
                post("/completeCreation", UserEndpoint.completeOauth);
            });
            get("/getProfile", UserEndpoint.getProfile);
        });
    }
}
