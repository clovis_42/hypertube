package org.hypertube.tools.exceptions;

import org.hypertube.tools.StatusResponse;

public class UserException extends Exception {

    StatusResponse statusResponse;

    public UserException(StatusResponse statusResponse) {
        this.statusResponse = statusResponse;
    }

    public StatusResponse getStatusResponse() {
        return statusResponse;
    }
}
