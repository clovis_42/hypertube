package org.hypertube.tools;

public enum StatusResponse {
    SUCCESS(200, "Request success"),
    EXISTS(221, "Entry is not available"),
    DOES_NOT_EXIST(242, "Entry is available"),
    /* CLIENT ERRORS */
    BAD_REQUEST(401, "Invalid entries."),
    AUTH_FAILED(401, "Username and / or password is incorrect."),
    AUTHENTICATION_FAILED(401, "Authentication failed"),
    FT_REGISTRATION_IMPOSSIBLE(401, "Cannot sign up with 42 account: email and / or username already exists."),
    GH_REGISTRATION_IMPOSSIBLE(401, "Cannot sign up with GitHub account: username already exists."),
    UNAUTHORIZED_ACCESS(401, "Authentication is required to access to this page"),
    NOT_FOUND(404, "Not found"),
    DUPLICATE_USER(419, "This user already exists."),

    /* SERVER ERRORS */
    INTERNAL_SERVER_ERROR(500, "Internal server error"),
    DATABASE_CONNECTION(0, "Connection to the database failed."),
    DATABASE_SCRIPTING(1, "No creation script could be found.");

    private final int code;
    private final String description;

    StatusResponse(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getMessage() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }
}
