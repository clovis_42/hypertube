package org.hypertube.tools;

import org.hypertube.tools.exceptions.UserException;

import static org.hypertube.tools.StatusResponse.*;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

public class Encoder {

    private static final String PASSWORD_SALT = "HyperSaltPass";
    private static final String TOKEN_SALT = "HyperSaltToken";
    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";

    public static String encodePassword(String password) throws UserException {
        return hashString(password, PASSWORD_SALT);
    }

    public static String encodeToken(String Token) throws UserException {
        return hashString(Token, TOKEN_SALT);
    }

    private static String hashString(String string, String salt) throws UserException {
        try {
            PBEKeySpec spec = new PBEKeySpec(string.toCharArray(), salt.getBytes(StandardCharsets.UTF_8), 42421, 1500);
            SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
            byte[] hashBytes = skf.generateSecret(spec).getEncoded();
            return Base64.getEncoder().encodeToString(hashBytes);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static String getToken() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[32];
        secureRandom.nextBytes(randomBytes);
        return Base64.getEncoder().encodeToString(randomBytes);
    }
}
