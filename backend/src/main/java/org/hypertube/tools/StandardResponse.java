package org.hypertube.tools;

import spark.Response;

import static org.hypertube.tools.StatusResponse.SUCCESS;

public class StandardResponse {

    public static Response standardResponse(Response response, StatusResponse status) {
        response.type("text");
        response.status(status.getCode());
        if (!status.equals(SUCCESS)) {
            response.body(status.getMessage());
        }
        return response;
    }

    public static Response standardResponse(Response response, StatusResponse status, String data) {
        response.type("application/json");
        response.status(status.getCode());
        response.body(data);
        return response;
    }
}
