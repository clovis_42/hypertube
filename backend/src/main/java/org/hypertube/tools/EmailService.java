package org.hypertube.tools;

import org.hypertube.entity.Email;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailService {
    public static void sendEmail(Email email) throws MessagingException {

        // Put sender’s address
        final String FROM = ""; // Your service email
        final String EMAIL_USERNAME = ""; // Username of your service email account
        final String EMAIL_PASSWORD = ""; // Password of your service email account

        // Paste host address of the SMTP server
        final String HOST = "";

        final String FORMAT = "text/html; charset=utf-8";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", HOST);
        props.put("mail.smtp.port", "587");

        // Get the Session object.
        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(EMAIL_USERNAME, EMAIL_PASSWORD);
                    }
                });

        // Create a default MimeMessage object.
        Message message = new MimeMessage(session);

        // Set From: header field
        message.setFrom(new InternetAddress(FROM));

        // Set To: header field
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(email.getRecipient()));

        // Set Subject: header field
        message.setSubject(email.getObject());

        // Put the content of your messageimport org.hypertube.entity.User;

        message.setContent(email.getContent(), FORMAT);

        // Save Changes
        message.saveChanges();

        // Send message
        Transport.send(message);
    }
}
