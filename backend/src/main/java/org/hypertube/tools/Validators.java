package org.hypertube.tools;

import static java.util.regex.Pattern.matches;

import org.hypertube.dataProviders.database.UserDatabase;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.codec.binary.Base64;
import org.hypertube.entity.User;

public class Validators {

    public static boolean usernameValidator(String username) throws SQLException {
        return (matches("^[0-9a-zA-Z-_]+$", username) && !userExists(username));
    }

    public static boolean firstNameValidator(String firstName) {
        return (matches("^[A-Za-z-]+$", firstName) && !firstName.isEmpty());
    }

    public static boolean lastNameValidator(String lastName) {
        return (matches("^[A-Za-z- ]+$", lastName));
    }


    public static boolean passwordValidator(String password, String confirmPassword) {
        return ((password.equals(confirmPassword)) && matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).{12,30}$", password));
    }

    public static boolean emailValidator(String email) throws SQLException {
        return (matches("^(.+)@(.+)$", email) && !emailExists(email));
    }

    public static boolean pictureValidator(String pictureString) {
        try {
            String[] pictureArray = pictureString.split(",");
            String picture = pictureArray[1];
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(Base64.decodeBase64(picture)));
            if (image == null) {
                return false;
            }
        } catch (IOException ex) {
            ex.getCause();
            return false;
        }
        return true;
    }

    public static boolean userExists(String username) throws SQLException {
        UserDatabase dbUsage = new UserDatabase();
        return dbUsage.userExists("username", username);
    }

    public static boolean emailExists(String email) throws SQLException {
        UserDatabase dbUsage = new UserDatabase();
        return dbUsage.userExists("email", email);
    }

    public static boolean isSignUpFormValid(User user) throws SQLException {
        return usernameValidator(user.getUsername()) &&
                emailValidator(user.getEmail()) &&
                lastNameValidator(user.getLastname()) &&
                firstNameValidator(user.getFirstname()) &&
                passwordValidator(user.getPassword(), user.getConfirmedPassword()) &&
                pictureValidator(user.getPicture());
    }

    public static boolean isUpdateSettingsFormValid(User user, String currentEmail, String currentPicture) throws SQLException {
        return ((user.getEmail().equals(currentEmail)) || emailValidator(user.getEmail())) &&
                lastNameValidator(user.getLastname()) &&
                firstNameValidator(user.getFirstname()) &&
                (currentPicture.equals(user.getPicture()) || pictureValidator(user.getPicture()));
    }
}
