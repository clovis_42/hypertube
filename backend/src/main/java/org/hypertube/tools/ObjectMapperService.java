package org.hypertube.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hypertube.entity.Comment;
import org.hypertube.entity.User;

import java.util.ArrayList;

public class ObjectMapperService {

    public static User userObjectMapper(String body) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(body, User.class);
    }

    public static Comment commentObjectMapper(String body) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(body, Comment.class);
    }

    public static String userObjectMapperToString(User user) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(user);
    }

    public static String commentsArrayObjectMapperToString(ArrayList<Comment> comments) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(comments);
    }
}
