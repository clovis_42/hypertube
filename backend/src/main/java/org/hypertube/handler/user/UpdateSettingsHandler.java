package org.hypertube.handler.user;

import org.hypertube.entity.User;
import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.tools.Validators;
import org.hypertube.tools.exceptions.UserException;

import java.sql.SQLException;

import static org.hypertube.tools.Encoder.encodePassword;
import static org.hypertube.tools.StatusResponse.*;
import static org.hypertube.tools.Encoder.encodeToken;
import static org.hypertube.tools.Validators.isUpdateSettingsFormValid;

public class UpdateSettingsHandler {

    public static void updateSettings(User user) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            String hashedToken = encodeToken(user.getAuthToken());
            if (!userDB.checkUserToken(user.getUsername(), "authToken", hashedToken)) {
                throw new UserException(UNAUTHORIZED_ACCESS);
            }
            if (!isUpdateSettingsFormValid(user, userDB.getValueBy("email", "username",
                    user.getUsername()), userDB.getValueBy("picture", "username", user.getUsername()))) {
                throw new UserException(BAD_REQUEST);
            }
            if (!user.getPassword().isEmpty()) {
                if (Validators.passwordValidator(user.getPassword(), user.getConfirmedPassword())) {
                    String encodedPassword = encodePassword(user.getPassword());
                    user.setPassword(encodedPassword);
                } else {
                    throw new UserException(BAD_REQUEST);
                }
            } else {
                user.setPassword(userDB.getValueBy("password", "username", user.getUsername()));
            }
            userDB.updateSettings(user);
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }
}
