package org.hypertube.handler.user;

import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.User;
import org.hypertube.tools.exceptions.UserException;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.hypertube.tools.Encoder.encodeToken;
import static org.hypertube.tools.StatusResponse.INTERNAL_SERVER_ERROR;
import static org.hypertube.tools.StatusResponse.UNAUTHORIZED_ACCESS;

public class WatchedMovieHandler {

    public static void setWatchedMovie(String username, String authToken, String movieId, String userId) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            String hashedToken = encodeToken(authToken);
            if (!userDB.checkUserToken(username, "authToken", hashedToken)) {
                throw new UserException(UNAUTHORIZED_ACCESS);
            }
            User user = new User();
            user.setUserId(Integer.parseInt(userId));
            ArrayList<String> movies = userDB.getWatchedMoviesList(user);
            if (!movies.contains(movieId)) {
                userDB.setMovieWatched(userId, movieId);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }
}
