package org.hypertube.handler.user;

import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.User;
import org.hypertube.tools.Encoder;

import static org.hypertube.tools.StatusResponse.*;

import org.hypertube.tools.exceptions.UserException;

import java.sql.SQLException;
import java.util.ArrayList;

public class SignInHandler {

    public static User handleSignIn(User user) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            if (userDB.checkLogins(user.getUsername(), Encoder.encodePassword(user.getPassword()))) {
                return getUserData(user);
            } else {
                throw new UserException(AUTH_FAILED);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static User getUserData(User user) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            User userData;
            String authToken = updateToken(user.getUsername());
            userData = userDB.getUserData(user.getUsername());
            userData.setAuthToken(authToken);
            ArrayList<String> moviesList = userDB.getWatchedMoviesList(userData);
            userData.setWatchedMovies(moviesList);
            return userData;
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static String updateToken(String username) throws UserException, SQLException {
        UserDatabase userDB = new UserDatabase();
        String authToken = Encoder.getToken();
        String encodedToken = Encoder.encodeToken(authToken);
        userDB.updateToken(username, encodedToken);
        return authToken;
    }

}
