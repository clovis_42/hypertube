package org.hypertube.handler.user;

import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.User;
import org.hypertube.tools.Validators;
import org.hypertube.tools.exceptions.UserException;
import org.json.JSONObject;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.hypertube.tools.StatusResponse.*;
import static org.hypertube.handler.user.CreateUserHandler.sendValidationEmail;
import static org.hypertube.tools.Encoder.*;

public class OauthHandler {
    static String FT_TOKEN_URI = "https://api.intra.42.fr/oauth/token";
    static String GH_TOKEN_URI = "https://github.com/login/oauth/access_token";
    static String FT_DATA_URI = "https://api.intra.42.fr/v2/me";
    static String GH_DATA_URI = "https://api.github.com/user";


    public static User fortyTwoAuth(String code) throws UserException {
        try {
            UserDatabase dbUser = new UserDatabase();
            String accessToken = getAccessToken(code, "ft");
            User UserData = getOauthUserData(accessToken, "ft");
            if (dbUser.userExists("ftId", String.valueOf(UserData.getFtId()))) {
                UserData.setUsername(dbUser.getUsername("ftId", UserData.getFtId()));
                return SignInHandler.getUserData(UserData);
            } else if (dbUser.userExists("email", UserData.getEmail())
                    || dbUser.userExists("username", UserData.getUsername())) {
                throw new UserException(FT_REGISTRATION_IMPOSSIBLE);
            } else {
                dbUser.insertFtUser(UserData);
                return SignInHandler.getUserData(UserData);
            }
        } catch (IOException | SQLException | InterruptedException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static User gitHubAuth(String code) throws UserException {
        try {
            UserDatabase dbUser = new UserDatabase();
            String accessToken = getAccessToken(code, "gh");
            User userData = getOauthUserData(accessToken, "gh");
            if (dbUser.userExists("gitHubId", String.valueOf(userData.getGhId()))) {
                if (dbUser.userIsConfirmed("gitHubId", String.valueOf(userData.getGhId()))) {
                    return SignInHandler.getUserData(userData);
                } else {
                    userData.setAuthToken(SignInHandler.updateToken(userData.getUsername()));
                    userData.setEmail(dbUser.getValueBy("email", "gitHubId", String.valueOf(userData.getGhId())));
                }
            } else if (dbUser.userExists("username", userData.getUsername())) {
                throw new UserException(GH_REGISTRATION_IMPOSSIBLE);
            } else {
                dbUser.insertGitHubUser(userData);
                userData.setAuthToken(SignInHandler.updateToken(userData.getUsername()));
            }
            return userData;
        } catch (SQLException exception) {
            throw new UserException(DATABASE_CONNECTION);
        } catch (IOException | InterruptedException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static void completeGitHubProfile(User user) throws UserException {
        try {
            UserDatabase dbUser = new UserDatabase();
            String hashedAuthToken = encodeToken(user.getAuthToken());
            if (dbUser.checkUserToken(user.getUsername(), "authToken", hashedAuthToken)) {
                if (Validators.emailValidator(user.getEmail()) &&
                        Validators.firstNameValidator(user.getFirstname()) &&
                        Validators.lastNameValidator(user.getLastname())) {
                    String emailToken = getToken();
                    user.setEmailToken(emailToken);
                    dbUser.updateGitHubProfile(user, encodeToken(emailToken));
                    sendValidationEmail(user.getUsername(), user.getEmail(), emailToken);
                } else {
                    throw new UserException(BAD_REQUEST);
                }
            } else {
                throw new UserException(AUTHENTICATION_FAILED);
            }
        } catch (SQLException | MessagingException | UnsupportedEncodingException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    private static User getOauthUserData(String accessToken, String provider) throws IOException, InterruptedException {
        HttpClient httpclient = HttpClient.newBuilder().build();
        var UserRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(provider.equals("ft") ? FT_DATA_URI : GH_DATA_URI))
                .setHeader("User-Agent", "Java 11 HttpClient Bot")
                .setHeader("Authorization", "Bearer " + accessToken)
                .build();
        HttpResponse<String> userDataResponse = httpclient.send(UserRequest, HttpResponse.BodyHandlers.ofString());
        JSONObject UserData = new JSONObject(userDataResponse.body());
        return provider.equals("ft") ? getFtUserData(UserData) : getGhUserData(UserData);
    }

    private static User getFtUserData(JSONObject UserData) {
        return new User(UserData.getString("login"),
                UserData.getString("first_name"),
                UserData.getString("last_name"),
                UserData.getString("image_url"),
                UserData.getString("email"),
                UserData.getInt("id"));
    }

    private static User getGhUserData(JSONObject UserData) {
        return new User(UserData.getString("login"),
                UserData.getString("avatar_url"),
                UserData.getInt("id"));
    }

    private static String getAccessToken(String code, String provider) throws SQLException, IOException, InterruptedException, UserException {
        HttpClient httpclient = HttpClient.newBuilder().build();
        HttpRequest TokenRequest = getTokenRequest(code, provider);
        HttpResponse<String> tokenResponse = httpclient.send(TokenRequest, HttpResponse.BodyHandlers.ofString());
        JSONObject TokenData = new JSONObject(tokenResponse.body());
        if (TokenData.has("error")) {
            throw new UserException(BAD_REQUEST);
        }
        return TokenData.getString("access_token");
    }

    private static HttpRequest getTokenRequest(String code, String provider) {
        Map<Object, Object> data = getRequestData(code, provider);
        return HttpRequest.newBuilder()
                .POST(buildFormDataFromMap(data))
                .uri(URI.create(provider.equals("ft") ? FT_TOKEN_URI : GH_TOKEN_URI))
                .setHeader("User-Agent", "Java 11 HttpClient Bot")
                .setHeader("Accept", "application/json")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .build();
    }

    private static Map<Object, Object> getRequestData(String code, String provider) {
        Map<Object, Object> data = new HashMap<>();
        if (provider.equals("ft")) {
            data.put("grant_type", "authorization_code");
            data.put("client_id", ""); // TODO fill the client_id parameter with your id
            data.put("client_secret", ""); // TODO fill the client_secret parameter with your id
            data.put("redirect_uri", "http://localhost:4200/Oauth42");
            data.put("code", code);
        } else if (provider.equals("gh")) {
            data.put("client_id", ""); // TODO fill the client_id parameter with your id
            data.put("client_secret", ""); // TODO fill the client_secret parameter with your id
            data.put("redirect_uri", "http://localhost:4200/OauthGh");
            data.put("code", code);
        }
        return data;
    }

    private static HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }
}
