package org.hypertube.handler.user;

import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.Email;
import org.hypertube.entity.User;
import org.hypertube.tools.exceptions.UserException;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import static org.hypertube.tools.Encoder.*;
import static org.hypertube.tools.EmailService.sendEmail;
import static org.hypertube.tools.StatusResponse.*;
import static org.hypertube.tools.Validators.isSignUpFormValid;

public class CreateUserHandler {

    public static void createUser(User user) throws UserException {
        try {
            if (isSignUpFormValid(user)) {
                String emailToken = insertUser(user);
                sendValidationEmail(user.getUsername(), user.getEmail(), emailToken);
            } else {
                throw new UserException(BAD_REQUEST);
            }
        } catch (SQLException | UnsupportedEncodingException | MessagingException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    private static String insertUser(User user) throws UserException {
        try {
            UserDatabase dbUserAction = new UserDatabase();
            String passwordHash = encodePassword(user.getPassword());
            String emailToken = getToken();
            String emailTokenHash = encodeToken(emailToken);
            dbUserAction.createUser(user, passwordHash, emailTokenHash);
            return emailToken;
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static void confirmAccount(User user) throws UserException {
        try {
            UserDatabase dbUserAction = new UserDatabase();
            String emailTokenHash = encodeToken(user.getEmailToken());
            if (dbUserAction.checkUserToken(user.getUsername(), "emailToken", emailTokenHash)) {
                dbUserAction.confirmAccount(user.getUsername(), 1);
            } else {
                throw new UserException(AUTHENTICATION_FAILED);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static void sendValidationEmail(String username, String recipient, String emailToken) throws UnsupportedEncodingException, MessagingException {
        String encodedToken;
        encodedToken = URLEncoder.encode(emailToken, StandardCharsets.UTF_8.toString());
        String link = "http://localhost:4200/confirm-account?username=" + username + "&token=" + encodedToken;
        String content = "Hi " + username + ", to validate your Hypertube account, click <a href='" + link + "'>here</a> ";
        String object = "Hypertube | Account validation";
        sendEmail(new Email(username, recipient, object, content));
    }

}
