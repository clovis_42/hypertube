package org.hypertube.handler.user;

import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.Email;
import org.hypertube.entity.User;
import org.hypertube.tools.exceptions.UserException;

import static org.hypertube.tools.Encoder.*;
import static org.hypertube.tools.EmailService.sendEmail;
import static org.hypertube.tools.StatusResponse.INTERNAL_SERVER_ERROR;
import static org.hypertube.tools.Validators.passwordValidator;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

public class ResetPasswordHandler {

    public static void resetPassword(User user) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            if (userDB.userExists("email", user.getEmail()) &&
                    userDB.getValueBy("ftId", "email", user.getEmail()) == null &&
                    userDB.getValueBy("gitHubId", "email", user.getEmail()) == null) {
                user.setUsername(userDB.getValueBy("username", "email", user.getEmail()));
                String emailToken = getToken();
                String hashedEmailToken = encodeToken(emailToken);
                userDB.updateEmailToken(hashedEmailToken, user.getEmail());
                sendResetPasswordEmail(user.getUsername(), user.getEmail(), emailToken);
            }
        } catch (SQLException | UnsupportedEncodingException | MessagingException e) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    private static void sendResetPasswordEmail(String username, String recipient, String emailToken)
            throws UnsupportedEncodingException, MessagingException {
        String encodedToken;
        encodedToken = URLEncoder.encode(emailToken, StandardCharsets.UTF_8.toString());
        String link = "http://localhost:4200/update-password?username=" + username + "&token=" + encodedToken;
        String content = "Hi" + username + ", to reset your password, please click on the following <a href='" + link + "'>link</a> ";
        String object = "Hypertube | Reset password";
        Email email = new Email(username, recipient, object, content);
        sendEmail(email);
    }

    public static void updatePassword(User user) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            String hashedEmailToken = encodeToken(user.getEmailToken());
            if (passwordValidator(user.getPassword(), user.getConfirmedPassword())
                    && userDB.checkUserToken(user.getUsername(), "emailToken", hashedEmailToken)) {
                String hashedPassword = encodePassword(user.getPassword());
                userDB.updatePassword(user.getUsername(), hashedPassword);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }
}
