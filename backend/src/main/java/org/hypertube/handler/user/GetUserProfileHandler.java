package org.hypertube.handler.user;

import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.User;

import static org.hypertube.tools.StatusResponse.INTERNAL_SERVER_ERROR;
import static org.hypertube.tools.StatusResponse.AUTHENTICATION_FAILED;
import static org.hypertube.tools.StatusResponse.NOT_FOUND;

import org.hypertube.tools.exceptions.UserException;

import static org.hypertube.tools.Encoder.encodeToken;

import java.sql.SQLException;


public class GetUserProfileHandler {
    public static User getProfileData(String profileId, String username, String token) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            if (userDB.checkUserToken(username, "authToken", encodeToken(token))) {
                if (userDB.userExists("userId", profileId)) {
                    return userDB.getProfileData(Integer.parseInt(profileId));
                } else {
                    throw new UserException(NOT_FOUND);
                }
            } else {
                throw new UserException(AUTHENTICATION_FAILED);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }
}
