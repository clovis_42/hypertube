package org.hypertube.handler.comments;

import org.hypertube.dataProviders.database.CommentDatabase;
import org.hypertube.dataProviders.database.UserDatabase;
import org.hypertube.entity.Comment;
import org.hypertube.tools.exceptions.UserException;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.hypertube.tools.Encoder.encodeToken;
import static org.hypertube.tools.StatusResponse.*;

public class CommentHandler {
    public static void insertComment(Comment comment, String authToken) throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            if (userDB.checkUserToken(comment.getUsername(), "authToken", encodeToken(authToken))) {
                if (comment.getContent().length() > 241) {
                    throw new UserException(BAD_REQUEST);
                }
                CommentDatabase commentDatabase = new CommentDatabase();
                commentDatabase.insertComment(comment);
            } else {
                throw new UserException(AUTHENTICATION_FAILED);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }

    public static ArrayList<Comment> getCommentsByMovie(String movieId, String username, String authToken)
            throws UserException {
        try {
            UserDatabase userDB = new UserDatabase();
            if (userDB.checkUserToken(username, "authToken", encodeToken(authToken))) {
                CommentDatabase commentDatabase = new CommentDatabase();
                return commentDatabase.getCommentsBy("movieId", movieId);
            } else {
                throw new UserException(AUTHENTICATION_FAILED);
            }
        } catch (SQLException exception) {
            throw new UserException(INTERNAL_SERVER_ERROR);
        }
    }
}
