package org.hypertube.endpoints;

import org.hypertube.handler.user.*;
import org.hypertube.tools.ObjectMapperService;
import org.hypertube.tools.exceptions.UserException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hypertube.entity.User;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;

import static org.hypertube.tools.StatusResponse.*;
import static org.hypertube.tools.StandardResponse.standardResponse;
import static org.hypertube.tools.ObjectMapperService.*;
import static org.hypertube.handler.user.WatchedMovieHandler.*;

public class UserEndpoint {

    public static Route signIn = (Request request, Response response) -> {
        User user;
        try {
            user = userObjectMapper(request.body());
            User userData = SignInHandler.handleSignIn(user);
            return standardResponse(response, SUCCESS, userObjectMapperToString(userData));
        } catch (JsonProcessingException exception) {
            return standardResponse(response, AUTH_FAILED);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
    };

    public static Route createUser = (Request request, Response response) -> {
        User user;
        try {
            user = userObjectMapper(request.body());
            CreateUserHandler.createUser(user);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route confirmAccount = (Request request, Response response) -> {
        User user;
        try {
            user = ObjectMapperService.userObjectMapper(request.body());
            CreateUserHandler.confirmAccount(user);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, INTERNAL_SERVER_ERROR);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route resetPassword = (Request request, Response response) -> {
        try {
            User user = userObjectMapper(request.body());
            ResetPasswordHandler.resetPassword(user);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route updatePassword = (Request request, Response response) -> {
        try {
            User user = userObjectMapper(request.body());
            ResetPasswordHandler.updatePassword(user);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route oauthConnection = (Request request, Response response) -> {
        ObjectMapper objectMapper = new ObjectMapper();
        JSONObject data = new JSONObject(request.body());
        String provider = data.getString("provider");
        String code = data.getString("code");
        User user = new User();
        try {
            if (provider.equals("ft")) {
                user = OauthHandler.fortyTwoAuth(code);
            } else if (provider.equals("gh")) {
                user = OauthHandler.gitHubAuth(code);
            }
            return standardResponse(response, SUCCESS, objectMapper.writeValueAsString(user));
        } catch (JsonProcessingException exception) {
            return standardResponse(response, INTERNAL_SERVER_ERROR);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
    };

    public static Route updateSettings = (Request request, Response response) -> {
        User user;
        try {
            user = userObjectMapper(request.body());
            UpdateSettingsHandler.updateSettings(user);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route completeOauth = (Request request, Response response) -> {
        User user;
        try {
            user = userObjectMapper(request.body());
            OauthHandler.completeGitHubProfile(user);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route getProfile = (Request request, Response response) -> {
        String profileId = request.queryParams("id");
        String username = request.queryParams("username");
        String authToken = request.headers("Authorization");
        User user;
        try {
            user = GetUserProfileHandler.getProfileData(profileId, username, authToken);
            return standardResponse(response, SUCCESS, userObjectMapperToString(user));
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
    };

    public static Route setMovieWatched = (Request request, Response response) -> {
        JSONObject data = new JSONObject(request.body());
        String movieId = data.getString("movieId");
        String userId = data.getString("userId");
        String username = data.getString("username");
        String authToken = request.headers("Authorization");
        try {
            setWatchedMovie(username, authToken, movieId, userId);
            return standardResponse(response, SUCCESS);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
    };
}
