package org.hypertube.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.hypertube.entity.Comment;
import org.hypertube.handler.comments.CommentHandler;
import org.hypertube.tools.exceptions.UserException;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;

import static org.hypertube.tools.ObjectMapperService.commentsArrayObjectMapperToString;
import static org.hypertube.tools.ObjectMapperService.commentObjectMapper;
import static org.hypertube.tools.StandardResponse.standardResponse;
import static org.hypertube.tools.StatusResponse.*;

public class CommentEndpoint {

    public static Route createComment = (Request request, Response response) -> {
        try {
            Comment comment = commentObjectMapper(request.body());
            String authToken = request.headers("Authorization");
            CommentHandler.insertComment(comment, authToken);
        } catch (JsonProcessingException exception) {
            return standardResponse(response, BAD_REQUEST);
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        }
        return standardResponse(response, SUCCESS);
    };

    public static Route getComments = (Request request, Response response) -> {
        String movieId = request.queryParams("movieId");
        String username = request.queryParams("username");
        String authToken = request.headers("Authorization");
        try {
            ArrayList<Comment> comments = CommentHandler.getCommentsByMovie(movieId, username, authToken);
            return standardResponse(response, SUCCESS, commentsArrayObjectMapperToString(comments));
        } catch (UserException exception) {
            return standardResponse(response, exception.getStatusResponse());
        } catch (JsonProcessingException exception) {
            return standardResponse(response, INTERNAL_SERVER_ERROR);
        }
    };

}
