package org.hypertube.endpoints;

import spark.Request;
import spark.Response;
import spark.Route;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static org.hypertube.tools.StandardResponse.standardResponse;
import static org.hypertube.tools.StatusResponse.SUCCESS;

public class CorsRequestEndpoint {

    public static Route getCorsRequest = (Request request, Response response) -> {
        String url = getUrl(request);
        HttpClient httpclient = HttpClient.newBuilder().build();
        var userRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url))
                .build();
        HttpResponse<String> userRequestResponse = httpclient.send(userRequest, HttpResponse.BodyHandlers.ofString());
        return standardResponse(response, SUCCESS, userRequestResponse.body());
    };

    private static String getUrl(Request request) {
        StringBuilder url = new StringBuilder(request.queryParams("url"));
        if (!url.toString().contains(new StringBuffer("?"))) {
            url.append('?');
        }
        for (String param : request.queryParams()) {
            if (!param.equals("url")) {
                if (url.charAt(url.length() - 1) != '?') {
                    url.append('&');
                }
                String encodedQueryStringValue = URLEncoder.encode(request.queryParams(param), StandardCharsets.UTF_8);
                url.append(param).append('=').append(encodedQueryStringValue);
            }
        }
        return url.toString();
    }
}
