package org.hypertube.endpoints;

import org.hypertube.configuration.database.DatabaseManagement;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import static org.hypertube.tools.StatusResponse.DATABASE_CONNECTION;
import static org.hypertube.tools.StatusResponse.DATABASE_SCRIPTING;

public class DatabaseManagementEndPoint {

    public static Route initDB = (Request request, Response response) -> {
        try {
            org.hypertube.configuration.database.DatabaseManagement manager = new DatabaseManagement();
            manager.createDatabase();
            manager.createUser();
            manager.createTables();
        } catch (FileNotFoundException notFoundException) {
            return DATABASE_SCRIPTING;
        } catch (SQLException exception) {
            return DATABASE_CONNECTION;
        }
        return "The database was successfully created";
    };
}
