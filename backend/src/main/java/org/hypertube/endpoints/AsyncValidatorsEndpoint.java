package org.hypertube.endpoints;

import org.hypertube.tools.Validators;
import spark.Request;
import spark.Response;
import spark.Route;

import java.sql.SQLException;

import static org.hypertube.tools.Validators.userExists;
import static org.hypertube.tools.StatusResponse.*;
import static org.hypertube.tools.StandardResponse.standardResponse;

public class AsyncValidatorsEndpoint {

    public static Route usernameExists = (Request request, Response response) -> {
        String username = request.queryParams("username");
        try {
            if (userExists(username)) {
                return standardResponse(response, EXISTS);
            }
            return standardResponse(response, DOES_NOT_EXIST);
        } catch (SQLException exception) {
            return standardResponse(response, INTERNAL_SERVER_ERROR);
        }
    };

    public static Route emailExists = (Request request, Response response) -> {
        String email = request.queryParams("email");
        String actualEmail = request.queryParams("actualEmail");
        try {
            if (!email.equals(actualEmail) && Validators.emailExists(email)) {
                return standardResponse(response, EXISTS);
            }
            return standardResponse(response, DOES_NOT_EXIST);
        } catch (SQLException exception) {
            return standardResponse(response, INTERNAL_SERVER_ERROR);
        }
    };
}
