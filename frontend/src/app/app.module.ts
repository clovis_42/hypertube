import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SignInFormComponent } from './components/sign-in-form/sign-in-form.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmAccountComponent } from './components/confirm-account/confirm-account.component';
import { MainComponent } from './components/main/main.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderButtonsComponent } from './components/header/header-buttons/header-buttons.component';
import { ContentComponent } from './components/content/content.component';
import { HomeComponent } from './components/home/home.component';
import { CookieService } from 'ngx-cookie-service';
import { Oauth42Component } from './components/oauth42/oauth42.component';
import { OauthGhComponent } from './components/oauth-gh/oauth-gh.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { UpdatePasswordComponent } from './components/update-password/update-password.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BannerComponent } from './components/banner/banner.component';
import { UpdateProfileComponent } from './components/update-profile/update-profile.component';
import { LibraryComponent } from './components/library/library.component';
import { FiltersComponent } from './components/filters/filters.component';
import { MovieComponent } from './components/movie/movie.component';
import { ArrayToStringPipe } from './helper/array-to-string-pipe';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    SignInFormComponent,
    SignupFormComponent,
    ConfirmAccountComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    HeaderButtonsComponent,
    ContentComponent,
    HomeComponent,
    Oauth42Component,
    OauthGhComponent,
    ResetPasswordComponent,
    UpdatePasswordComponent,
    ProfileComponent,
    BannerComponent,
    UpdateProfileComponent,
    LibraryComponent,
    FiltersComponent,
    MovieComponent,
    ArrayToStringPipe
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        TranslateModule.forRoot({
            defaultLanguage: 'en',
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ReactiveFormsModule,
        FormsModule
    ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
