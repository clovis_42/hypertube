import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookiesService } from '../../helper/services/cookies/cookies.service';
import { UserService } from '../../helper/services/apiServices/user.service';
import { Banner, Oauth, User } from '../../helper/interfaces';

@Component({
  selector: 'app-oauth42',
  templateUrl: './oauth42.component.html',
  styleUrls: ['./oauth42.component.sass']
})
export class Oauth42Component implements OnInit {
  @Output() SuccessEvent = new EventEmitter();
  @Output() bannerEvent = new EventEmitter();

  returnState: string;
  storeState: string;
  code: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private cookiesService: CookiesService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.returnState = params.state;
        this.code = params.code;
      });
    this.storeState = this.cookiesService.getCookie('ftState');
    if (this.storeState === this.returnState) {
      const oauth: Oauth = {
        provider: 'ft',
        code: this.code
      };
      this.userService.oauthConnection(oauth).subscribe(response => {
        const user: User = response.body;
        this.cookiesService.setUserCookie(user);
        this.router.navigate(['']);
      }, error => {
        let banner: Banner;
        switch (error.status) {
          case 401:
            banner = {
              success: false,
              message: 'ERROR_BANNER.FT_REGISTRATION_IMPOSSIBLE',
              display: true
            };
            break;
          case 500:
            banner = {
              success: false,
              message: 'ERROR_BANNER.INTERNAL_SERVER',
              display: true
            };
            break;
          default:
            banner = {
              success: false,
              message: 'ERROR_BANNER.STANDARD',
              display: true
            };
        }
        this.bannerEvent.emit(banner);
        setTimeout(() => {
          window.location.assign('/');
        }, 5000);
      });
    }
  }
}
