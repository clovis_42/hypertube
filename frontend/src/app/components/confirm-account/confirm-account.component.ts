import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../helper/services/apiServices/user.service';
import { User } from '../../helper/interfaces';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.sass']
})

export class ConfirmAccountComponent implements OnInit {
  username: string;
  token: string;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
          this.username = params.username;
          this.token = params.token;
        }
      );

    const user: User = {
      username: this.username,
      emailToken: this.token
    };
    this.userService.confirmAccount(user).subscribe(() => {
    });
  }

}
