import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.sass']
})

export class FiltersComponent {
  filtersDisplayed = false;
  sortByDropdown = false;
  orderDropdown = false;
  genreDropdown = false;
  maxYear = 2021;
  minYear = 1900;

  @Output() sortSubjectEvent = new EventEmitter();
  @Output() sortOrderEvent = new EventEmitter();
  @Output() genreFilterEvent = new EventEmitter();
  @Output() YearFromEvent = new EventEmitter();
  @Output() YearToEvent = new EventEmitter();


  constructor() {
  }

  public displayFilters(): void {
    this.filtersDisplayed = !this.filtersDisplayed;
  }

  public displaySortCat(): void {
    this.sortByDropdown = !this.sortByDropdown;
  }

  public displayOrder(): void {
    this.orderDropdown = !this.orderDropdown;
  }

  public displayType(): void {
    this.genreDropdown = !this.genreDropdown;
    this.sortSubject('title');
  }

  public sortSubject(subject: string): void {
    this.sortSubjectEvent.emit(subject);
  }

  public sortOrder(order: string): void {
    this.sortOrderEvent.emit(order);
  }

  public setGenreFilter(event): void {
    this.genreFilterEvent.emit(event.target.value);
  }

  public setFrom(event): void {
    let from: number = event.target.value;
    if (!from) {
      from = this.minYear;
    }
    if (from <= this.maxYear && from >= this.minYear) {
      this.YearFromEvent.emit(from);
    }
  }

  public setTo(event): void {
    let to: number = event.target.value;
    if (!to) {
      to = this.maxYear;
    }
    if (to <= this.maxYear && to >= this.minYear) {
      this.YearToEvent.emit(to);
    }
  }
}
