import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../helper/interfaces';
import {UserService} from '../../helper/services/apiServices/user.service';
import {CookiesService} from '../../helper/services/cookies/cookies.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})

export class ProfileComponent implements OnInit {

  @Input() profileId: string;
  @Output() userProfile: EventEmitter<void> = new EventEmitter();
  @Output() backHome: EventEmitter<void> = new EventEmitter();
  picture: string;
  username: string;
  firstname: string;
  lastname: string;
  user: User;

  constructor(public userService: UserService,
              public cookiesService: CookiesService) {
  }

  onUserProfile(): void {
    this.userProfile.emit();
  }

  ngOnInit(): void {
    this.userService.getUserProfile(this.profileId, this.cookiesService.getCookie('username')).subscribe(response => {
      const userData: User = response.body;
      this.picture = userData.picture;
      this.username = userData.username;
      this.firstname = userData.firstname;
      this.lastname = userData.lastname;
    });
  }

  onBackHome(): void {
    this.backHome.emit();
  }

}
