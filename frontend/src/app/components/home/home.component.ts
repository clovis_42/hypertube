import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {State} from '../../helper/interfaces';
import {UserState} from '../../helper/enum';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  @Output() userProfile = new EventEmitter();
  @Output() signIn = new EventEmitter();
  @Output() library = new EventEmitter();
  @Input() state: State;


  ngOnInit(): void {
    if (!this.isConnected()){
      this.signIn.emit();
    } else {
      this.library.emit();
    }
  }

  isConnected(): boolean {
    return this.state.userState === UserState.CONNECTED;
  }
}
