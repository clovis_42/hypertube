import {Component, EventEmitter, Output} from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { EmailExists } from '../../helper/validators/emailExists';
import { PswMatch } from '../../helper/validators/passwordMatch';
import { UserExists } from '../../helper/validators/usernameExists';
import { requiredFileType } from '../../helper/validators/requireFileTypeValidator';
import { fileSizeValidator } from '../../helper/validators/fileSizeValidator';
import { UserService } from '../../helper/services/apiServices/user.service';
import {Banner, User} from '../../helper/interfaces';
import {OauthService} from '../../helper/services/oAuth/oauth.service';
import {ResponseStatus} from '../../helper/enum';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.sass']
})

export class SignupFormComponent {
  @Output() bannerEvent = new EventEmitter();
  public invalidSize: boolean;
  public formLocker = false;
  oauthFtLink: string;
  oauthGhLink: string;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private oauthService: OauthService) {
    this.oauthFtLink = this.oauthService.getFtLink();
    this.oauthGhLink = this.oauthService.getGhLink();
  }

  picture = '';

  get username(): AbstractControl | null {
    return this.signUpForm.get('username');
  }

  get firstname(): AbstractControl | null {
    return this.signUpForm.get('firstname');
  }

  get lastname(): AbstractControl | null {
    return this.signUpForm.get('lastname');
  }

  get email(): AbstractControl | null {
    return this.signUpForm.get('email');
  }

  get password(): AbstractControl | null {
    return this.signUpForm.get('password');
  }

  get confirmPassword(): AbstractControl | null {
    return this.signUpForm.get('confirmPassword');
  }

  get fileUpload(): AbstractControl | null {
    return this.signUpForm.get('fileUpload');
  }

  signUpForm = this.fb.group({
    username: ['', [
      Validators.required,
      Validators.pattern('^[0-9a-zA-Z-_]+$'),
      Validators.maxLength(50)
    ], UserExists(this.userService)
    ],
    firstname: ['', [
      Validators.required,
      Validators.pattern('^[A-Za-z-]+$'),
      Validators.maxLength(50)
    ]],
    lastname: ['', [
      Validators.required,
      Validators.pattern('^[A-Za-z- ]+$'),
      Validators.maxLength(50)
    ]],
    email: ['', [
      Validators.required,
      Validators.email
    ], EmailExists(this.userService, '')
    ],
    password: ['', [
      Validators.required,
      Validators.minLength(12),
      Validators.maxLength(30),
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).{12,30}$')
    ]],
    confirmPassword: ['', Validators.required],
    fileUpload: ['', requiredFileType]
  }, {validators: PswMatch});

  private static getFileSize(file: any): number | null {
    let fileSize: number;
    if (file) {
      fileSize = file.size;
      return Math.round(fileSize / 1024);
    }
    return null;
  }

  insertPic(event: any): void {
    const reader = new FileReader();
    const file = event.target.files[0];
    const fileSize = SignupFormComponent.getFileSize(file);
    this.invalidSize = fileSizeValidator(fileSize);
    if (fileSize && !this.invalidSize) {
      reader.onload = () => {
        this.picture = reader.result as string ;
      };
      reader.readAsDataURL(file);
    }
  }

  deletePic(): void {
    this.picture = '';
  }

  submitUserForm(): void {
    if (!this.formLocker){
      this.formLocker = true;
      const user: User = {
        username: this.username.value,
        lastname: this.lastname.value,
        firstname: this.firstname.value,
        password: this.password.value,
        confirmedPassword: this.confirmPassword.value,
        email: this.email.value,
        picture: this.picture
      };
      this.userService.createUser(user).subscribe(() => {
        const banner: Banner = {
          success: true,
          message: 'SUCCESS_BANNER.SIGN_UP',
          display: true
        };
        this.bannerEvent.emit(banner);
      }, error => {
        let banner: Banner;
        switch (error.status) {
          case ResponseStatus.BAD_REQUEST:
            banner = {
              success: false,
              message: 'ERROR_BANNER.BAD_REQUEST',
              display: true
            };
            break;
          case ResponseStatus.INTERNAL_SERVER:
            banner = {
              success: false,
              message: 'ERROR_BANNER.INTERNAL_SERVER',
              display: true
            };
            break;
          default:
            banner = {
              success: false,
              message: 'ERROR_BANNER.STANDARD',
              display: true
            };
        }
        this.bannerEvent.emit(banner);
      });
    }
  }
}
