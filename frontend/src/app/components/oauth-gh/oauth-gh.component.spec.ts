import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OauthGhComponent } from './oauth-gh.component';

describe('OauthGhComponent', () => {
  let component: OauthGhComponent;
  let fixture: ComponentFixture<OauthGhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OauthGhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OauthGhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
