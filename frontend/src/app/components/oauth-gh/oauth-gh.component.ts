import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CookiesService} from '../../helper/services/cookies/cookies.service';
import {UserService} from '../../helper/services/apiServices/user.service';
import {Banner, Oauth, User} from '../../helper/interfaces';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {EmailExists} from '../../helper/validators/emailExists';

@Component({
  selector: 'app-oauth-gh',
  templateUrl: './oauth-gh.component.html',
  styleUrls: ['./oauth-gh.component.sass']
})
export class OauthGhComponent implements OnInit {
  @Output() bannerEvent = new EventEmitter();
  returnState: string;
  storeState: string;
  code: string;
  partialUser: User;
  displayForm = false;
  displayEmailMessage = false;
  actualEmail = '';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private cookiesService: CookiesService,
              private userService: UserService,
              private fb: FormBuilder) {
  }

  oAuthForm = this.fb.group({
    firstname: ['', [
      Validators.required,
      Validators.pattern('^[A-Za-z-]+$'),
      Validators.maxLength(50)
    ]],
    lastname: ['', [
      Validators.required,
      Validators.pattern('^[A-Za-z- ]+$'),
      Validators.maxLength(50)
    ]],
    email: ['', [
      Validators.required,
      Validators.email
    ]]
  });

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.returnState = params.state;
        this.code = params.code;
      });
    this.storeState = this.cookiesService.getCookie('ghState');
    if (this.storeState === this.returnState) {
      const oauth: Oauth = {
        provider: 'gh',
        code: this.code
      };
      this.userService.oauthConnection(oauth).subscribe(response => {
        this.partialUser = response.body;
        if (this.partialUser.confirmed) {
          this.cookiesService.setUserCookie(response.body);
          this.router.navigate(['']);
        } else {
          this.actualEmail = this.partialUser.email;
          this.displayForm = true;
          this.email.setAsyncValidators(EmailExists(this.userService, this.actualEmail));
        }
      }, error => {
        let banner: Banner;
        switch (error.status) {
          case 401:
            banner = {
              success: false,
              message: 'ERROR_BANNER.GH_REGISTRATION_IMPOSSIBLE',
              display: true
            };
            break;
          case 500:
            banner = {
              success: false,
              message: 'ERROR_BANNER.INTERNAL_SERVER',
              display: true
            };
            break;
          default:
            banner = {
              success: false,
              message: 'ERROR_BANNER.STANDARD',
              display: true
            };
        }
        this.bannerEvent.emit(banner);
        setTimeout(() => {
          window.location.assign('/');
        }, 5000);
      });
    }
  }

  get firstname(): AbstractControl | null {
    return this.oAuthForm.get('firstname');
  }

  get lastname(): AbstractControl | null {
    return this.oAuthForm.get('lastname');
  }

  get email(): AbstractControl | null {
    return this.oAuthForm.get('email');
  }

  submitOauthForm(): void {
    const user: User = {
      username: this.partialUser.username,
      authToken: this.partialUser.authToken,
      email: this.email.value,
      firstname: this.firstname.value,
      lastname: this.lastname.value,
      ghId: this.partialUser.ghId
    };
    this.userService.completeOauthProfile(user).subscribe(response => {
      if (response.status === 200) {
        this.displayForm = false;
        this.displayEmailMessage = true;
      }
    });
  }
}
