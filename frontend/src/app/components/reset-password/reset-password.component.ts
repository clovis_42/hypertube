import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {Banner, User} from '../../helper/interfaces';
import {UserService} from '../../helper/services/apiServices/user.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.sass']
})

export class ResetPasswordComponent implements OnInit {

  @Output() bannerEvent = new EventEmitter();

  constructor(private userService: UserService,
              private formBuilder: FormBuilder) {
  }

  get email(): AbstractControl | null {
    return this.resetPasswordForm.get('email');
  }

  resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    }
  );

  ngOnInit(): void {
  }

  submitResetPwdForm(): void {
    const user: User = {
      email: this.email.value
    };
    this.userService.resetPassword(user).subscribe(() => {
      const banner: Banner = {
        success: true,
        message: 'SUCCESS_BANNER.RESET_PWD',
        display: true
      };
      this.bannerEvent.emit(banner);
    }, error => {
      let banner: Banner;
      switch (error.status) {
        case 401:
          banner = {
            success: false,
            message: 'ERROR_BANNER.BAD_REQUEST',
            display: true
          };
          break;
        case 500:
          banner = {
            success: false,
            message: 'ERROR_BANNER.INTERNAL_SERVER',
            display: true
          };
          break;
        default:
          banner = {
            success: false,
            message: 'ERROR_BANNER.STANDARD',
            display: true
          };
      }
      this.bannerEvent.emit(banner);
    });
  }
}
