import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Banner, Comment, Thumbnails} from '../../helper/interfaces';
import {Observable} from 'rxjs';
import {CommentService} from '../../helper/services/apiServices/comment.service';
import {CookiesService} from '../../helper/services/cookies/cookies.service';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {SubtitlesService} from '../../helper/services/subtitles/subtitles.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ResponseStatus} from '../../helper/enum';
import {UserService} from '../../helper/services/apiServices/user.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.sass']
})

export class MovieComponent implements OnInit {
  @Output() bannerEvent = new EventEmitter();
  @Output() profile = new EventEmitter();
  @Output() unauthorizedAccess = new EventEmitter();
  @Input() movie: Thumbnails;

  isAuthorized = true;
  hasFrSubtitles = false;
  hasEnSubtitles = false;

  constructor(private commentService: CommentService,
              private userService: UserService,
              private cookiesService: CookiesService,
              private subtitlesService: SubtitlesService,
              private Sanitizer: DomSanitizer,
              private formBuilder: FormBuilder) {
  }

  get inputComment(): AbstractControl | null {
    return this.commentForm.get('inputComment');
  }

  movieUrl: string;
  comments: Observable<Comment[]>;
  streamingEndPoint = 'http://localhost:3000/movie/stream?';
  userComments: Comment[] = [];
  enSub: SafeResourceUrl;
  frSub: SafeResourceUrl;

  commentForm = this.formBuilder.group({
    inputComment: ['', [
      Validators.required,
      Validators.maxLength(241)
    ]]
  });

  ngOnInit(): void {
    this.movieWatched();
    if (this.isAuthorized) {
      this.movieUrl = this.streamingEndPoint
        + 'imdbId=' + this.movie.IMDbId
        + '&archiveId=' + this.movie.ArchiveId
        + '&ytsHash=' + this.movie.YTSHash
        + '&title=' + this.movie.title;
      this.comments = this.displayComment(this.movie.IMDbId);
      const subArray = this.subtitlesService.getSubtitles(this.movie.IMDbId.toString());
      if (subArray) {
        subArray.then(subsArray => {
          if (subsArray[0] !== undefined) {
            this.hasFrSubtitles = true;
            this.frSub = this.Sanitizer.bypassSecurityTrustUrl(subsArray[0]);
          }
          if (subsArray[1] !== undefined) {
            this.hasEnSubtitles = true;
            this.enSub = this.Sanitizer.bypassSecurityTrustUrl(subsArray[1]);
          }
        });
      }
    }
  }

  movieWatched(): void {
    const userID = this.cookiesService.getCookie('userId');
    const username = this.cookiesService.getCookie('username');
    const moviesListAsString = this.cookiesService.getCookie('watchedMovies');
    const moviesList = moviesListAsString.split(',');
    if (!moviesList.includes(this.movie.IMDbId.toString())) {
      this.userService.setMovieWatched(this.movie.IMDbId.toString(), userID, username).subscribe(() => {
        moviesList.push(this.movie.IMDbId.toString());
        this.cookiesService.setCookie('watchedMovies', moviesList);
      }, error => {
        let banner: Banner;
        switch (error.status) {
          case ResponseStatus.AUTHENTICATION_FAILED:
            banner = {
              success: false,
              message: 'ERROR_BANNER.UNAUTHORIZED_ACCESS',
              display: true
            };
            this.isAuthorized = false;
            this.cookiesService.deleteUserCookie();
            this.unauthorizedAccess.emit();
            break;
          case ResponseStatus.INTERNAL_SERVER:
            banner = {
              success: false,
              message: 'ERROR_BANNER.SET_WATCHED',
              display: true
            };
            break;
          default:
            banner = {
              success: false,
              message: 'ERROR_BANNER.STANDARD',
              display: true
            };
        }
        this.bannerEvent.emit(banner);
      });
    }
  }

  displayComment(ImdbId: number | string): Observable<Comment[]> {
    return this.commentService.getCommentById(ImdbId.toString(), this.cookiesService.getCookie('username'));
  }

  onProfile(userId: number): void {
    this.profile.emit(userId);
  }

  sendComment(): void {
    const comment: Comment = {
      movieId: this.movie.IMDbId.toString(),
      userId: Number(this.cookiesService.getCookie('userId')),
      username: this.cookiesService.getCookie('username'),
      content: this.inputComment.value,
      createdAt: ''
    };
    if (this.commentForm.valid) {
      this.commentService.postComment(comment).subscribe(() => {
        this.userComments.push(comment);
        this.commentForm.reset();
      }, error => {
        let banner: Banner;
        switch (error.status) {
          case ResponseStatus.BAD_REQUEST:
            banner = {
              success: false,
              message: 'ERROR_BANNER.BAD_REQUEST',
              display: true
            };
            break;
          case ResponseStatus.INTERNAL_SERVER:
            banner = {
              success: false,
              message: 'ERROR_BANNER.INTERNAL_SERVER',
              display: true
            };
            break;
          default:
            banner = {
              success: false,
              message: 'ERROR_BANNER.STANDARD',
              display: true
            };
        }
        this.bannerEvent.emit(banner);
      });
    }
  }
}
