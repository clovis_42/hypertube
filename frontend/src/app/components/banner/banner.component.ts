import { Component, Input } from '@angular/core';
import { Banner } from '../../helper/interfaces';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.sass']
})
export class BannerComponent {
  @Input() banner: Banner;
}
