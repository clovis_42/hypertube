import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {PswMatch} from '../../helper/validators/passwordMatch';
import {Banner, User} from '../../helper/interfaces';
import {UserService} from '../../helper/services/apiServices/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.sass']
})

export class UpdatePasswordComponent implements OnInit {
  @Output() bannerEvent = new EventEmitter();

  emailToken: string;
  username: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private formBuilder: FormBuilder) {
  }

  get password(): AbstractControl | null {
    return this.updatePwdForm.get('password');
  }

  get confirmPassword(): AbstractControl | null {
    return this.updatePwdForm.get('confirmPassword');
  }

  updatePwdForm = this.formBuilder.group({
    password: ['', [
      Validators.required,
      Validators.minLength(12),
      Validators.maxLength(30),
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).{12,30}$')
    ]],
    confirmPassword: ['', Validators.required],
  }, {validators: PswMatch});

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.emailToken = params.token;
      this.username = params.username;
    });
  }

  submitUpdatePwdForm(): void {
    const user: User = {
      password: this.password.value,
      confirmedPassword: this.confirmPassword.value,
      emailToken: this.emailToken,
      username: this.username
    };
    this.userService.updatePassword(user).subscribe(() => {
      const banner: Banner = {
        success: true,
        message: 'SUCCESS_BANNER.UPDATE_PWD',
        display: true
      };
      this.bannerEvent.emit(banner);
    }, error => {
      let banner: Banner;
      switch (error.status) {
        case 401:
          banner = {
            success: false,
            message: 'ERROR_BANNER.BAD_REQUEST',
            display: true
          };
          break;
        case 500:
          banner = {
            success: false,
            message: 'ERROR_BANNER.INTERNAL_SERVER',
            display: true
          };
          break;
        default:
          banner = {
            success: false,
            message: 'ERROR_BANNER.STANDARD',
            display: true
          };
      }
      this.bannerEvent.emit(banner);
    });
  }

}
