import {Component, EventEmitter, Output} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../helper/services/apiServices/user.service';
import {CookiesService} from '../../helper/services/cookies/cookies.service';
import {Banner, User} from '../../helper/interfaces';
import {OauthService} from '../../helper/services/oAuth/oauth.service';

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.sass']
})

export class SignInFormComponent {
  @Output() bannerEvent = new EventEmitter();
  @Output() SuccessEvent = new EventEmitter();
  @Output() resetPwdEvent = new EventEmitter();

  oauthFtLink: string;
  oauthGhLink: string;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private cookiesService: CookiesService,
              private oauthService: OauthService) {
    this.oauthFtLink = this.oauthService.getFtLink();
    this.oauthGhLink = this.oauthService.getGhLink();
  }

  get username(): AbstractControl | null {
    return this.signInForm.get('username');
  }

  get password(): AbstractControl | null {
    return this.signInForm.get('password');
  }

  signInForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  public submitSignInForm(): void {
    const user: User = {
      username: this.username.value,
      password: this.password.value
    };
    this.userService.authenticateUser(user).subscribe(response => {
      const userData: User = response.body;
      userData.connected = true;
      this.cookiesService.setUserCookie(userData);
      this.SuccessEvent.emit();
    }, error => {
      let banner: Banner;
      switch (error.status) {
        case 401:
          banner = {
            success: false,
            message: 'ERROR_BANNER.AUTHENTICATION',
            display: true
          };
          break;
        case 500:
          banner = {
            success: false,
            message: 'ERROR_BANNER.INTERNAL_SERVER',
            display: true
          };
          break;
        default:
          banner = {
            success: false,
            message: 'ERROR_BANNER.STANDARD',
            display: true
          };
      }
      this.bannerEvent.emit(banner);
    });
  }

  public onResetPassword(): void {
    this.resetPwdEvent.emit();
  }
}
