import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {LibraryService} from '../../helper/services/library/library.service';
import {Thumbnails} from '../../helper/interfaces';
import {CookiesService} from '../../helper/services/cookies/cookies.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.sass']
})
export class LibraryComponent implements OnInit {
  @Output() movie = new EventEmitter();
  page = 1;
  moviesList: Thumbnails[];
  titleSearch = '';
  orderSort = 'desc';
  sortSubject = '';
  filterGenre = '';
  from = 1900;
  to = 2021;
  loadingSearch = false;
  loadingLibrary = true;
  loadingMore = false;
  isLoading = false;

  constructor(private moviesService: LibraryService,
              private cookiesService: CookiesService) {
  }

  async ngOnInit(): Promise<void> {
    this.isLoading = true;
    this.moviesList = await this.moviesService.getPopularMovies(this.page);
    this.loadingLibrary = false;
    this.isLoading = false;
  }

  public async OnBottomPage(): Promise<void> {
    this.loadingMore = true;
    this.isLoading = true;
    this.page = this.page + 1;
    this.moviesList = this.moviesList.concat(await this.moviesService.getPopularMovies(this.page));
    this.loadingMore = false;
    this.isLoading = false;
  }

  public async submitResearch(): Promise<void> {
    this.loadingSearch = true;
    if (!this.isLoading) {
      this.isLoading = true;
      if (this.titleSearch === '') {
        this.moviesList = await this.moviesService.getPopularMovies(this.page);
      } else {
        this.moviesList = await this.moviesService.getSearchResults(this.titleSearch);
        this.orderSort = 'ascending';
        this.sortBy('title');
      }
    }
    this.loadingSearch = false;
    this.isLoading = false;
  }

  public setSortOrder(order: string): void {
    this.orderSort = order;
    this.sortBy(this.sortSubject);
  }

  public sortBy(subject: string): void {
    this.sortSubject = subject;
    switch (subject) {
      case 'rating':
        this.moviesList.sort((movie1, movie2) => {
          if (movie1.IMDbRate < movie2.IMDbRate) {
            return this.orderSort === 'desc' ? 1 : -1;
          }
          if (movie1.IMDbRate > movie2.IMDbRate) {
            return this.orderSort === 'desc' ? -1 : 1;
          }
          return 0;
        });
        break;

      case 'title':
        this.moviesList.sort((movie1, movie2) => {
          if (movie1.title < movie2.title) {
            return this.orderSort === 'desc' ? 1 : -1;
          }
          if (movie1.title > movie2.title) {
            return this.orderSort === 'desc' ? -1 : 1;
          }
          return 0;
        });
        break;

      case 'year':
        this.moviesList.sort((movie1, movie2) => {
          if (movie1.year < movie2.year) {
            return this.orderSort === 'desc' ? 1 : -1;
          }
          if (movie1.year > movie2.year) {
            return this.orderSort === 'desc' ? -1 : 1;
          }
          return 0;
        });
        break;

      default:
        this.moviesList.sort((movie1, movie2) => {
          if (movie1.downloads < movie2.downloads) {
            return this.orderSort === 'desc' ? 1 : -1;
          }
          if (movie1.downloads > movie2.downloads) {
            return this.orderSort === 'desc' ? -1 : 1;
          }
          return 0;
        });
    }
  }

  public filterBy(genre): void {
    this.filterGenre = genre;
  }

  public isFilterGenre(genres: Array<string>): boolean {
    return genres.includes(this.filterGenre) || this.filterGenre === '';
  }

  public isFilterYear(date: string): boolean {
    const year = Number.parseInt(date, 10);
    return year <= this.to && year >= this.from;
  }

  public setFromYear(year: number): void {
    this.from = year;
  }

  public setToYear(year: number): void {
    this.to = year;
  }

  public isWatched(IMDbId): boolean {
    const moviesListAsString = this.cookiesService.getCookie('watchedMovies');
    const moviesList = moviesListAsString.split(',');
    return moviesList.includes(IMDbId);
  }

  public setMovie(movie: Thumbnails): void {
    this.movie.emit(movie);
  }
}
