import {Component, OnInit} from '@angular/core';
import {Banner, State} from '../../helper/interfaces';
import {PageState, UserState} from '../../helper/enum';
import {ActivatedRoute} from '@angular/router';
import {CookiesService} from '../../helper/services/cookies/cookies.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {
  state: State;
  path: string;
  banner: Banner;

  constructor(private route: ActivatedRoute,
              private cookiesService: CookiesService) {
  }

  ngOnInit(): void {
    this.route.url.subscribe(url => {
      if (url.length) {
        this.path = url[0].path;
      } else {
        this.path = '';
      }
    });
    if (this.cookiesService.getCookie('connected') && this.cookiesService.getCookie('confirmed')) {
      this.state = {
        userState: UserState.CONNECTED,
        pageState: PageState.HOME
      };
    } else {
      this.state = {
        userState: UserState.DISCONNECTED,
        pageState: PageState.HOME
      };
    }
    this.banner = {
      success: true,
      message: '',
      display: false
    };
  }

  onSignIn(): void {
    this.path = '';
    this.state = {
      ...this.state,
      pageState: PageState.SIGN_IN
    };
  }

  onSignUp(): void {
    this.path = '';
    this.state = {
      ...this.state,
      pageState: PageState.SIGN_UP
    };
  }

  setState(state: State): void {
    this.path = '';
    this.state = state;
  }

  onLogout(): void {
    this.cookiesService.deleteUserCookie();
    this.state = {
      userState: UserState.DISCONNECTED,
      pageState: PageState.SIGN_IN
    };
  }

  onSettings(): void {
    this.state = {
      ...this.state,
      pageState: PageState.SETTINGS
    };
  }

  onHome(): void {
    this.state = {
      ...this.state,
      pageState: PageState.HOME
    };
  }
}
