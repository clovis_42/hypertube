import {Component, EventEmitter, Output} from '@angular/core';
import {fileSizeValidator} from '../../helper/validators/fileSizeValidator';
import {PswMatch} from '../../helper/validators/passwordMatch';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {EmailExists} from '../../helper/validators/emailExists';
import {UserService} from 'src/app/helper/services/apiServices/user.service';
import {CookiesService} from '../../helper/services/cookies/cookies.service';
import {requiredFileType} from '../../helper/validators/requireFileTypeValidator';
import {Banner, User} from '../../helper/interfaces';
import {ResponseStatus} from '../../helper/enum';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.sass']
})
export class UpdateProfileComponent {
  @Output() bannerEvent = new EventEmitter();
  @Output() unauthorizedAccess = new EventEmitter();

  public invalidSize: boolean;
  public formLocker = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private cookiesService: CookiesService) {
  }

  isOauth = this.cookiesService.getCookie('ftId') !== '0' || this.cookiesService.getCookie('ghId') !== '0';
  picture = this.cookiesService.getCookie('picture');

  username = this.cookiesService.getCookie('username');

  get firstname(): AbstractControl | null {
    return this.updateProfileForm.get('firstname');
  }

  get lastname(): AbstractControl | null {
    return this.updateProfileForm.get('lastname');
  }

  get email(): AbstractControl | null {
    return this.updateProfileForm.get('email');
  }

  get password(): AbstractControl | null {
    return this.updateProfileForm.get('password');
  }

  get confirmPassword(): AbstractControl | null {
    return this.updateProfileForm.get('confirmPassword');
  }

  get fileUpload(): AbstractControl | null {
    return this.updateProfileForm.get('fileUpload');
  }

  updateProfileForm = this.formBuilder.group({
    firstname: [this.cookiesService.getCookie('firstname'), [
      Validators.required,
      Validators.pattern('^[A-Za-z-]+$'),
      Validators.maxLength(50)
    ]],
    lastname: [this.cookiesService.getCookie('lastname'), [
      Validators.required,
      Validators.pattern('^[A-Za-z- ]+$'),
      Validators.maxLength(50)
    ]],
    email: [this.cookiesService.getCookie('email'), [
      Validators.required,
      Validators.email
    ], EmailExists(this.userService, this.cookiesService.getCookie('email'))
    ],
    password: ['', [
      Validators.minLength(12),
      Validators.maxLength(30),
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).{12,30}$')
    ]],
    confirmPassword: [''],
    fileUpload: ['', requiredFileType]
  }, {validators: PswMatch});

  private static getFileSize(file: any): number | null {
    let fileSize: number;
    if (file) {
      fileSize = file.size;
      return Math.round(fileSize / 1024);
    }
    return null;
  }

  insertPic(event: any): void {
    const reader = new FileReader();
    const file = event.target.files[0];
    const fileSize = UpdateProfileComponent.getFileSize(file);
    this.invalidSize = fileSizeValidator(fileSize);
    if (fileSize && !this.invalidSize) {
      reader.onload = () => {
        this.picture = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  deletePic(): void {
    this.picture = '';
  }

  submitUpdateProfileForm(): void {
    if (!this.formLocker) {
      this.formLocker = true;
      const user: User = {
        username: this.cookiesService.getCookie('username'),
        lastname: this.lastname.value,
        firstname: this.firstname.value,
        password: this.password.value,
        confirmedPassword: this.confirmPassword.value,
        email: this.email.value,
        picture: this.picture,
        authToken: this.cookiesService.getCookie('authToken')
      };
      this.userService.updateProfile(user).subscribe(() => {
        const banner: Banner = {
          success: true,
          message: 'SUCCESS_BANNER.UPDATE_PROFILE',
          display: true
        };
        this.bannerEvent.emit(banner);
        this.cookiesService.setUserCookie(user);
        this.formLocker = false;
      }, error => {
        let banner: Banner;
        this.formLocker = false;
        switch (error.status) {
          case ResponseStatus.AUTHENTICATION_FAILED:
            banner = {
              success: false,
              message: 'ERROR_BANNER.UNAUTHORIZED_ACCESS',
              display: true
            };
            this.cookiesService.deleteUserCookie();
            this.unauthorizedAccess.emit();
            break;
          case ResponseStatus.BAD_REQUEST:
            banner = {
              success: false,
              message: 'ERROR_BANNER.BAD_REQUEST',
              display: true
            };
            break;
          case ResponseStatus.INTERNAL_SERVER:
            banner = {
              success: false,
              message: 'ERROR_BANNER.INTERNAL_SERVER',
              display: true
            };
            break;
          default:
            banner = {
              success: false,
              message: 'ERROR_BANNER.STANDARD',
              display: true
            };
        }
        this.bannerEvent.emit(banner);
      });
    }
  }
}
