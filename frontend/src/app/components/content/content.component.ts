import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Banner, State, Thumbnails} from '../../helper/interfaces';
import {PageState, UserState} from '../../helper/enum';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.sass']
})
export class ContentComponent {
  @Input() state: State;
  @Output() setState = new EventEmitter();
  @Output() userProfile = new EventEmitter();
  @Output() setBanner = new EventEmitter();
  movie: Thumbnails | null;
  userProfileId: string;

  onUserProfile(id: string): void {
    this.userProfile.emit(id);
  }

  isHome(): boolean {
    return this.state.pageState === PageState.HOME;
  }

  isSignIn(): boolean {
    return this.state.pageState === PageState.SIGN_IN;
  }

  isSignUp(): boolean {
    return this.state.pageState === PageState.SIGN_UP;
  }

  isResetPassword(): boolean {
    return this.state.pageState === PageState.RESET_PASSWORD;
  }

  isUserProfile(): boolean {
    return this.state.pageState === PageState.USER_PROFILE;
  }

  isUpdateProfile(): boolean {
    return this.state.pageState === PageState.SETTINGS;
  }

  isLibrary(): boolean {
    return this.state.pageState === PageState.LIBRARY;
  }

  isMovie(): boolean {
    return this.state.pageState === PageState.MOVIE;
  }

  authSuccess(): void {
    const successState: State = {
      userState: UserState.CONNECTED,
      pageState: PageState.HOME
    };
    this.setState.emit(successState);
  }

  displayBanner(banner: Banner): void {
    this.setBanner.emit(banner);
  }

  displayResetPwd(): void {
    const resetPasswordState: State = {
      userState: UserState.DISCONNECTED,
      pageState: PageState.RESET_PASSWORD
    };
    this.setState.emit(resetPasswordState);
  }

  setHome(): void {
    const homeState: State = {
      userState: this.state.userState,
      pageState: PageState.HOME
    };
    this.setState.emit(homeState);
  }

  setUnauthorizedAccess(): void {
    const homeState: State = {
      userState: UserState.DISCONNECTED,
      pageState: PageState.HOME
    };
    this.setState.emit(homeState);
  }

  setLibrary(): void {
    const homeState: State = {
      userState: this.state.userState,
      pageState: PageState.LIBRARY
    };
    this.setState.emit(homeState);
  }

  setSignIn(): void {
    const signInState: State = {
      userState: UserState.DISCONNECTED,
      pageState: PageState.SIGN_IN
    };
    this.setState.emit(signInState);
  }

  setMovie(movie): void {
    this.movie = movie;
    const movieState: State = {
      userState: this.state.userState,
      pageState: PageState.MOVIE
    };
    this.setState.emit(movieState);
  }

  setProfile(userId: number): void {
    this.userProfileId = Number(userId).toString(10);
    const profileState: State = {
      userState: this.state.userState,
      pageState: PageState.USER_PROFILE
    };
    this.setState.emit(profileState);
  }

}
