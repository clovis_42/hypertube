import {Component, EventEmitter, Input, Output} from '@angular/core';
import {State} from '../../helper/interfaces';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent {
  @Input() state: State;
  @Output() signIn = new EventEmitter();
  @Output() signUp = new EventEmitter();
  @Output() logout = new EventEmitter();
  @Output() settings = new EventEmitter();
  @Output() home = new EventEmitter();

  onSignIn(): void {
    this.signIn.emit();
  }

  onSignUp(): void {
    this.signUp.emit();
  }

  onLogout(): void {
    this.logout.emit();
  }

  onSetting(): void {
    this.settings.emit();
  }

  onHome(): void {
    this.home.emit();
  }
}
