import {Component, EventEmitter, Input, Output} from '@angular/core';
import {State} from '../../../helper/interfaces';
import {UserState} from '../../../helper/enum';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-header-buttons',
  templateUrl: './header-buttons.component.html',
  styleUrls: ['./header-buttons.component.sass']
})

export class HeaderButtonsComponent {
  @Input() state: State;
  @Output() signIn: EventEmitter<void> = new EventEmitter();
  @Output() signUp: EventEmitter<void> = new EventEmitter();
  @Output() logout: EventEmitter<void> = new EventEmitter();
  @Output() profile: EventEmitter<void> = new EventEmitter();

  constructor(private translateService: TranslateService) {
  }

  public onChangeLanguage(): void {
    if (this.translateService.currentLang === 'en' || this.translateService.currentLang === undefined) {
      this.translateService.use('fr');
    } else {
      this.translateService.use('en');
    }
  }

  onSignIn(): void {
    this.signIn.emit();
  }

  onSignUp(): void {
    this.signUp.emit();
  }

  onLogout(): void {
    this.logout.emit();
  }

  onProfile(): void {
    this.profile.emit();
  }

  isUserConnected(): boolean {
    return this.state.userState === UserState.CONNECTED;
  }
}
