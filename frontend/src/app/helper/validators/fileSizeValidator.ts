export function fileSizeValidator(fileSize: any): boolean {
  return !(fileSize && fileSize >= 15 && fileSize <= 2000);
}
