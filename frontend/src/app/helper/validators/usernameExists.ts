import { AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../services/apiServices/user.service';

export function UserExists(userService: UserService): AsyncValidatorFn {
  return (control: AbstractControl):
    Promise<ValidationErrors | null> |
    Observable<ValidationErrors | null> => {
    return userService.getUsernameExists(control.value).pipe(
      map((UserExist) => (UserExist ? {userExists: true} : null))
    );
  };
}
