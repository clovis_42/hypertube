import { AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../services/apiServices/user.service';


export function EmailExists(userService: UserService, actualEmail: string): AsyncValidatorFn {
  return (control: AbstractControl):
    Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    return userService.getEmailExists(control.value, actualEmail).pipe(
      map((EmailExist) => (EmailExist ? {emailExists: true} : null))
    );
  };
}
