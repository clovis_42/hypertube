import { AbstractControl } from '@angular/forms';

export function requiredFileType(control: AbstractControl): {[key: string]: boolean } | null {
  const file: string = control.value;
  const fileExtensions: string[] = ['.jpg', '.jpeg', '.png'];
  if ((!file) || (file && fileExtensions.filter(value => file.endsWith(value)).length > 0)) {
    return null;
  }
  return { invalidType: true };
}
