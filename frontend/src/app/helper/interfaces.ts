import { PageState, UserState } from './enum';

export interface State {
  userState: UserState;
  pageState: PageState;
}

export interface Oauth {
  provider: string;
  code: string;
}

export interface Parameter {
  key: string;
  value: string;
}

export interface User {
  userId?: number;
  firstname?: string;
  lastname?: string;
  username?: string;
  password?: string;
  confirmedPassword?: string;
  email?: string;
  authToken?: string;
  emailToken?: string;
  picture?: string;
  confirmed?: boolean;
  ftId?: number;
  ghId?: number;
  connected?: boolean;
  watchedMovies?: Array<string>;
}

export interface Banner {
  success: boolean;
  message: string;
  display: boolean;
}

export interface ArchiveResponse {
  response: {
    docs: Array<ArchiveMovie>
  };
  responseHeader: object;
}

export interface YtsResponse {
  status: string;
  status_message: string;
  data: {
    movie_count: number;
    limit: number;
    page_number;
    movies: YtsMovie[]
  };
}

export interface ArchiveMovie {
  identifier: string;
  title: string;
  year: number;
  downloads: number;
  OMDb: OMDbMovie;
}

export interface YtsMovie {
  id: number;
  url: string;
  imdb_code: string;
  title: string;
  year: number;
  OMDb: OMDbMovie;
  detail: YTSDetail;
  torrents: YTSTorrent[];
}

export interface Thumbnails {
  title: string;
  year: string;
  ArchiveId: string;
  YTSId: string;
  IMDbId: number | string;
  IMDbRate: number | string;
  cover: string;
  downloads: number;
  genres: Array<string>;
  YTSHash: string;
  OMDbData: OMDbMovie;
}

export interface YTSDetail {
  data: {
    movie: {
      download_count: number;
    };
  };
}

export interface YTSTorrent {
  url: string;
  hash: string;
  quality: string;
}

export interface OMDbMovie {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: Array<OMDbExtRating>;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  Production: string;
}

export interface OMDbExtRating {
  Source: string;
  Value: string;
}

export interface Comment {
  movieId: string;
  userId: number;
  username: string;
  createdAt: string;
  content: string;
}

export interface Subtitles {
  en: string;
  fr: string;
}

export interface MovieWatched {
  movieId: string;
  userId: string;
  username: string;
}
