import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Subtitles} from '../../interfaces';
import * as url from 'url';

@Injectable({
  providedIn: 'root'
})
export class SubtitlesService {

  constructor(public httpClient: HttpClient) {
  }

  public async getSubtitles(imdbId: string): Promise<Array<url>> {
    const openSubtitlesLinks: Subtitles = await this.getOpenSubtitlesLink(imdbId);
    if (openSubtitlesLinks) {
      const frSubs = await this.getSubtitlesURL(openSubtitlesLinks.fr);
      const enSubs = await this.getSubtitlesURL(openSubtitlesLinks.en);
      return [frSubs, enSubs];
    }
    return [];
  }

  private async getOpenSubtitlesLink(imdbId: string): Promise<Subtitles> {
    let openSubtitlesObject: Subtitles;
    const promise: Promise<HttpResponse<any>> = this.httpClient.get(
      'http://localhost:3000/subtitles/get?imdbId=' + imdbId,
      {observe: 'response', responseType: 'json'})
      .toPromise();
    openSubtitlesObject = await promise.then(response => {
      return response.body;
    }, error => {
      console.log(error);
    });
    return openSubtitlesObject;
  }

  private async getSubtitlesURL(link: string): url {
    const promise: any = this.httpClient.get(
      link, {responseType: 'text'})
      .toPromise();
    return await promise.then(text => {
      const blob = new Blob([text], {type: 'text/vtt'});
      return URL.createObjectURL(blob);
    }, () => {
      console.log('Error: opensubtitles could not provide all subtitles.');
    });
  }
}
