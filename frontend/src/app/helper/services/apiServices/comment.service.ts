import {Observable} from 'rxjs';
import {Comment, Parameter} from '../../interfaces';
import {HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentService extends ApiService {

  getCommentById(movieId: string, username: string): Observable<Comment[]> {
    const args: Array<Parameter> = [{
      key: 'username',
      value: username
    }, {
      key: 'movieId',
      value: movieId
    }];
    return this.get('comment/get', args, true).pipe(map(response => {
      return response.body;
    }));
  }

  postComment(comment: Comment): Observable<HttpResponse<object>> {
    return this.post('comment/post', JSON.stringify(comment), false);
  }
}
