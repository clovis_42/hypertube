import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {HttpResponse} from '@angular/common/http';
import {Parameter} from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class MoviesProvidersService extends ApiService {
  OMDB_API_KEY = ''; // TODO fill with your own OMDB Api key

  public getArchivePopular(page: string): Promise<HttpResponse<object>> {
    return this.httpClient.get(
      'https://archive.org/advancedsearch.php?q=collection%3A%28feature_films%29+AND+mediatype%3A%28movies%29&fl%5B%5D=identifier&fl%5B%5D=title&fl%5B%5D=year&fl%5B%5D=downloads&sort%5B%5D=downloads+desc&rows=20&page='
      + page + '&output=json&save=yes',
      {observe: 'response', responseType: 'json'})
      .toPromise();
  }

  public getYtsPopular(page: string): Promise<HttpResponse<object>> {
    const args: Array<Parameter> = [{
      key: 'url',
      value: 'https://yts.proxyninja.org/api/v2/list_movies.json'
    },
      {
        key: 'sort_by',
        value: 'download_count'
      },
      {
        key: 'page',
        value: page
      }];
    return this.get('corsRequest', args, true).toPromise();
  }

  public getOMDbDetailsByTitle(title): Promise<HttpResponse<any>> {
    return this.httpClient.get(
      'http://www.omdbapi.com/?apikey=' + this.OMDB_API_KEY + '&t=' + title,
      {observe: 'response', responseType: 'json'})
      .toPromise();
  }

  public getOMDbDetailsById(id): Promise<HttpResponse<any>> {
    return this.httpClient.get(
      'http://www.omdbapi.com/?apikey=' + this.OMDB_API_KEY + '&i=' + id,
      {observe: 'response', responseType: 'json'})
      .toPromise();
  }

  public getYtsDetail(id): Promise<HttpResponse<object>> {
    const args: Array<Parameter> = [{
      key: 'url',
      value: 'https://yts.proxyninja.org/api/v2/movie_details.json'
    },
      {
        key: 'movie_id',
        value: id
      }];
    return this.get('corsRequest', args, true).toPromise();
  }

  public ytsSearch(title): Promise<HttpResponse<object>> {
    const args: Array<Parameter> = [{
      key: 'url',
      value: 'https://yts.proxyninja.org/api/v2/list_movies.json'
    },
      {
        key: 'sort_by',
        value: title
      },
      {
        key: 'query_term',
        value: title
      }];
    return this.get('corsRequest', args, true).toPromise();
  }

  public archiveSearch(title): Promise<HttpResponse<object>> {
    return this.httpClient.get(
      'https://archive.org/advancedsearch.php?q=title%3A('
      + title
      + ')+AND+collection%3A(feature_films)+AND+mediatype%3A(movies)&fl[]=identifier&fl[]=title&fl[]=year&sort[]=titleSorter+desc&rows=20&page=1&output=json&save=yes',
      {observe: 'response', responseType: 'json'})
      .toPromise();
  }
}
