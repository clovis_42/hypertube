import { TestBed } from '@angular/core/testing';

import { MoviesProvidersService } from './movies-providers.service';

describe('MoviesProvidersService', () => {
  let service: MoviesProvidersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoviesProvidersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
