import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {MovieWatched, Oauth, Parameter, User} from '../../interfaces';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UserService extends ApiService {

  public createUser(user: User): Observable<number> {
    return this.post('user/createUser', JSON.stringify(user), false).pipe(map(response => {
      return response.status;
    }));
  }

  public getEmailExists(email: string, actualEmail: string): Observable<boolean> {
    const args: Array<Parameter> = [{
      key: 'email',
      value: email
    },
      {
        key: 'actualEmail',
        value: actualEmail
      }];
    return this.get('validators/emailExists', args, false).pipe(map(response => {
      return response.status === 221;
    }));
  }

  public getUsernameExists(username: string): Observable<boolean> {
    const args: Array<Parameter> = [{
      key: 'username',
      value: username
    }];
    return this.get('validators/usernameExists', args, false).pipe(map(response => {
      return response.status === 221 ;
    }));
  }

  public confirmAccount(user: User): Observable<number> {
    return this.post('user/confirmAccount', JSON.stringify(user), false).pipe(map(response => {
      return response.status;
    }));
  }

  public authenticateUser(user: User): Observable<HttpResponse<object>> {
    return this.post('user/authenticate', JSON.stringify(user), true);
  }

  public oauthConnection(oauth: Oauth): Observable<HttpResponse<object>> {
    return this.post('user/oauth/connection', JSON.stringify(oauth), true);
  }

  public completeOauthProfile(user: User): Observable<HttpResponse<object>> {
    return this.post('user/oauth/completeCreation', JSON.stringify(user), false);
  }

  public resetPassword(user: User): Observable<HttpResponse<object>> {
    return this.post('user/password/postReset', JSON.stringify(user), false);
  }

  public updatePassword(user: User): Observable<HttpResponse<object>> {
    return this.post('user/password/postUpdate', JSON.stringify(user), false);
  }

  public updateProfile(user: User): Observable<HttpResponse<object>> {
    return this.post('user/update', JSON.stringify(user), false);
  }

  public getUserProfile(id: string, username: string): Observable<HttpResponse<object>> {
    const args: Array<Parameter> = [{
      key: 'id',
      value: id
    }, {
      key: 'username',
      value: username
    }];
    return this.get('user/getProfile', args, true);
  }

  public setMovieWatched(movieId: string, userId: string, username: string): Observable<HttpResponse<object>> {
    const movieWatched: MovieWatched = {
      movieId,
      userId,
      username
    };
    return this.post('user/movieWatched', JSON.stringify(movieWatched), false);
  }
}
