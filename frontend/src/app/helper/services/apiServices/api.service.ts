import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Parameter } from '../../interfaces';
import { CookiesService } from '../cookies/cookies.service';

@Injectable({
  providedIn: 'root'
})

export abstract class ApiService {
  private URL = 'http://localhost:4567/';

  constructor(public httpClient: HttpClient, public cookiesService: CookiesService) {
  }

  protected post(endpoint: string, data: string, returnJson: boolean): Observable<HttpResponse<any>> {
    const authToken = this.cookiesService.getCookie('authToken');
    let header = new HttpHeaders();
    if (authToken != null) {
      header = new HttpHeaders({
        Authorization: authToken
      });
    }
    if (returnJson) {
      return this.httpClient.post(this.URL + endpoint, data, {
        observe: 'response',
        responseType: 'json',
        headers: header
      });
    } else {
      return this.httpClient.post(this.URL + endpoint, data, {
        observe: 'response',
        responseType: 'text',
        headers: header
      });
    }
  }

  protected get(endpoint: string, args: Array<Parameter>, returnJson: boolean): Observable<HttpResponse<any>> {
    const parameters = this.getParameters(args);
    let header = new HttpHeaders();
    const authToken = this.cookiesService.getCookie('authToken');
    if (authToken != null) {
      header = new HttpHeaders({
        Authorization: authToken
      });
    }
    if (returnJson) {
      return this.httpClient.get(this.URL + endpoint + parameters, {
        observe: 'response',
        responseType: 'json',
        headers: header
      });
    } else {
      return this.httpClient.get(this.URL + endpoint + parameters, {
        observe: 'response',
        responseType: 'text',
        headers: header
      });
    }
  }

  private getParameters(args: Array<Parameter>): string {
    let Parameters = '?';
    args.forEach(parameter => {
      Parameters += parameter.key + '=' + parameter.value + '&';
    });
    if (Parameters.endsWith('&')) {
      Parameters = Parameters.slice(0, Parameters.length - 1);
    }
    return Parameters;
  }
}
