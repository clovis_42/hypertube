import {Injectable} from '@angular/core';
import {CryptoService} from '../crypto/crypto.service';
import {CookiesService} from '../cookies/cookies.service';

@Injectable({
  providedIn: 'root'
})
export class OauthService {
  private oauthFtLink = 'https://api.intra.42.fr/oauth/authorize?redirect_uri=http%3A%2F%2Flocalhost%3A4200%2FOauth42&response_type=code&scope=public&client_id=&state='; // TODO fill the client_id parameter with your id
  private oauthGhLink = 'https://github.com/login/oauth/authorize?redirect_uri=http%3A%2F%2Flocalhost%3A4200%2FOauthGh&client_id=&state='; // TODO fill the client_id parameter with your id

  constructor(private crypto: CryptoService,
              private cookiesService: CookiesService) {
  }

  public getFtLink(): string {
    return this.oauthFtLink + this.getState('ftState');
  }

  public getGhLink(): string {
    return this.oauthGhLink + this.getState('ghState');
  }

  private getState(provider: string): string {
    const state = this.crypto.randomString(42);
    this.cookiesService.setCookie(provider, state);
    return state;
  }
}
