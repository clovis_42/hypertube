import {Injectable} from '@angular/core';
import {MoviesProvidersService} from '../apiServices/movies-providers.service';
import {ArchiveMovie, ArchiveResponse, Thumbnails, YtsMovie, YtsResponse} from '../../interfaces';
import {HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  constructor(private moviesApi: MoviesProvidersService) {
  }

  private static formatGenres(genresList: string): Array<string> {
    return genresList.split(', ');
  }

  public async getPopularMovies(page: number): Promise<Thumbnails[]> {
    let list = [];
    const YTSList = await this.getYtsPopular(page);
    const ArchiveList = await this.getArchivePopular(page);
    list = list.concat(YTSList, ArchiveList);
    list.sort((movie1, movie2) => {
        if (movie1.downloads < movie2.downloads) {
          return 1;
        }
        if (movie1.downloads > movie2.downloads) {
          return -1;
        }
        return 0;
      }
    );
    return list;
  }

  private async getYtsPopular(page: number): Promise<Thumbnails[]> {
    return this.getYtsList(this.moviesApi.getYtsPopular(page.toString()));
  }

  private async getArchivePopular(page: number): Promise<Thumbnails[]> {
    return await this.getArchiveList(this.moviesApi.getArchivePopular(page.toString()));
  }

  public async getSearchResults(title: string): Promise<Thumbnails[]> {
    const list = [];
    const ArchiveList = await this.getArchiveResearchList(title);
    const YTSList = await this.getYtsResearchList(title);
    return list.concat(YTSList, ArchiveList);
  }

  private async getYtsResearchList(title: string): Promise<Thumbnails[]> {
    return this.getYtsList(this.moviesApi.ytsSearch(title));
  }

  private async getArchiveResearchList(title: string): Promise<Thumbnails[]> {
    return this.getArchiveList(this.moviesApi.archiveSearch(title));
  }

  private async getYtsList(request: Promise<HttpResponse<object>>): Promise<Thumbnails[]> {
    const ytsMoviesList = await this.getYtsMoviesList(request);
    return await this.completeYtsMoviesList(ytsMoviesList);
  }

  private async getYtsMoviesList(request: Promise<HttpResponse<object>>): Promise<YtsMovie[] | void> {
    return request.then(success => {
      const ytsResponse: YtsResponse = JSON.parse(JSON.stringify(success.body));
      return ytsResponse.data.movies;
    }, fail => {
      console.log(fail);
      console.log(fail.body);
    });
  }

  private async completeYtsMoviesList(moviesList: void | YtsMovie[]): Promise<Thumbnails[]> {
    const thumbnailsList: Thumbnails[] = [];
    if (moviesList) {
      for (const movie of moviesList) {
        movie.OMDb = await this.moviesApi.getOMDbDetailsById(movie.imdb_code).then(success => {
          return success.body;
        }, () => {
          return null;
        });
        movie.detail = await this.moviesApi.getYtsDetail(movie.id).then(success => {
          return JSON.parse(JSON.stringify(success.body));
        });
        if (movie.detail != null && movie.OMDb != null && movie.OMDb.imdbID && movie.OMDb.imdbRating) {
          const thumbnail: Thumbnails = {
            title: movie.title,
            ArchiveId: '',
            YTSId: movie.id.toString(),
            year: movie.year.toString(),
            downloads: movie.detail.data.movie.download_count,
            cover: movie.OMDb.Poster.endsWith('jpg' || 'png' || 'jpeg') ? movie.OMDb.Poster : '',
            IMDbRate: movie.OMDb.imdbRating,
            IMDbId: movie.OMDb.imdbID,
            genres: LibraryService.formatGenres(movie.OMDb.Genre),
            YTSHash: movie.torrents[0].hash,
            OMDbData: movie.OMDb
          };
          thumbnailsList.push(thumbnail);
        }
      }
      return thumbnailsList;
    }
    return [];
  }

  private async getArchiveList(request: Promise<HttpResponse<object>>): Promise<Thumbnails[]> {
    const archiveMoviesList = await this.getArchiveMoviesList(request);
    return await this.completeArchiveMoviesList(archiveMoviesList);
  }

  private getArchiveMoviesList(request: Promise<HttpResponse<object>>): Promise<ArchiveMovie[] | void> {
    return request.then(success => {
      const archiveResponse: ArchiveResponse = JSON.parse(JSON.stringify(success.body));
      return archiveResponse.response.docs;
    });
  }

  private async completeArchiveMoviesList(moviesList: void | ArchiveMovie[]): Promise<Thumbnails[]> {
    const thumbnailsList: Thumbnails[] = [];
    if (moviesList) {
      for (const movie of moviesList) {
        movie.OMDb = await this.moviesApi.getOMDbDetailsByTitle(movie.title).then(success => {
          return success.body;
        }, () => {
          return null;
        });
        if (movie.OMDb != null && movie.OMDb.imdbID && movie.OMDb.imdbRating) {
          const thumbnail: Thumbnails = {
            title: movie.title,
            ArchiveId: movie.identifier,
            year: movie.OMDb.Year,
            downloads: movie.downloads,
            IMDbId: movie.OMDb.imdbID,
            cover: movie.OMDb.Poster.endsWith('jpg' || 'png' || 'jpeg') ? movie.OMDb.Poster : '',
            IMDbRate: movie.OMDb.imdbRating,
            YTSId: '',
            genres: LibraryService.formatGenres(movie.OMDb.Genre),
            YTSHash: '',
            OMDbData: movie.OMDb
          };
          thumbnailsList.push(thumbnail);
        }
      }
      return thumbnailsList;
    }
    return [];
  }

}
