import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class CookiesService {

  constructor(private cookieService: CookieService) { }

  setCookie(key: string, value: any): void {
    if (this.cookieService.check(key)) {
      this.cookieService.delete(key);
    }
    this.cookieService.set(key, value);
  }

  getCookie(key: string): string {
    if (key === 'picture') {
      return localStorage.getItem('picture');
    }
    if (this.cookieService.check(key)) {
      return this.cookieService.get(key);
    }
  }

  setUserCookie(user: User): void {
    this.setCookie('userId', user.userId);
    this.setCookie('firstname', user.firstname);
    this.setCookie('lastname', user.lastname);
    this.setCookie('username', user.username);
    this.setCookie('email', user.email);
    this.setCookie('authToken', user.authToken);
    localStorage.setItem('picture', user.picture);
    this.setCookie('confirmed', user.confirmed);
    this.setCookie('connected', user.connected);
    this.setCookie('ftId', user.ftId);
    this.setCookie('ghId', user.ghId);
    this.setCookie('watchedMovies', user.watchedMovies);
  }

  deleteUserCookie(): void {
    this.cookieService.deleteAll();
    localStorage.removeItem('picture');
}

}
