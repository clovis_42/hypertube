export enum UserState {
  CONNECTED = 'connected',
  DISCONNECTED = 'disconnected',
}

export enum PageState {
  HOME,
  SIGN_IN,
  SIGN_UP,
  RESET_PASSWORD,
  USER_PROFILE,
  SETTINGS,
  LIBRARY,
  MOVIE,
}

export enum ResponseStatus {
  BAD_REQUEST = 401,
  AUTHENTICATION_FAILED = 401,
  INTERNAL_SERVER = 500
}
