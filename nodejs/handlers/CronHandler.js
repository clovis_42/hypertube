const cron = require('node-cron');
const fs = require('fs');
const path = require("path");
const moviePath = path.normalize(__dirname + "../../../Downloads/Movies");
const torrentPath = path.normalize(__dirname + "../../../Downloads/Torrents");

function deleteDirectory(directoryPath, directory) {
    fs.rmdir(path.join(directoryPath, directory), {recursive: true}, function (err,) {
        if (err) {
            console.log(err)
        } else {
            console.log("Directory " + directory + " is deleted");
        }
    })
}

function deleteFile(directoryPath, file) {
    fs.unlink(path.join(directoryPath, file), (err) => {
        if (err) {
            console.log(err)
        } else {
            console.log("File " + file + " is deleted");
        }
    })
}

function cleaner(directoryPath, ageOfExpiration, creationCriteria) {
    fs.readdir(directoryPath, function (err, files) {
        const date = new Date();
        if (err) {
            console.log(err);
            return;
        }
        files.forEach(file => {
            const fileData = fs.statSync(path.join(directoryPath, file));
            const fileDate = new Date(creationCriteria ? fileData.ctime : fileData.atime);
            const ageInDate = Math.ceil(Math.abs(date.getTime() - fileDate.getTime()) / (1000 * 3600 * 24));
            if (ageInDate > ageOfExpiration) {
                if (fileData.isDirectory()) {
                    deleteDirectory(directoryPath, file);
                } else {
                    deleteFile(directoryPath, file);
                }
            }
        })
    })
}

function dailyCleaner() {
    cron.schedule('0 0 0 * * *', function () {
        cleaner(torrentPath, 1, true);
        cleaner(moviePath, 31, false);
    }, {})
}

module.exports = {dailyCleaner: dailyCleaner};
