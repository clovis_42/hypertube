const torrentStream = require("torrent-stream");
const torrentToMagnet = require("torrent-to-magnet");
const Transcoder = require('stream-transcoder');
const path = require("path");
const fs = require('fs');
const moviePath = path.normalize(__dirname + "../../../Downloads/Movies");

function setStreamParams(movieLength, range) {
    const params = {};
    params.ranges = range.split('=')[1].split('-');
    params.start = parseInt(params.ranges[0], 10);
    params.end = params.ranges[1] ? parseInt(params.ranges[1], 10) : movieLength - 1;
    params.size = movieLength;
    return params;
}

function writeHeader(res, status, params) {
    const header = {
        "Content-Range": `bytes ${params.start}-${params.end}/${params.size}`,
        "Accept-Ranges": "bytes",
        "Content-Length": params.end - params.start + 1,
        "Content-Type": "video/mp4"
    };
    res.writeHead(status, header);
    return res;
}

function streamDownloadingMovie(movie, range, res, toConvert) {
    if (range) {
        const streamParams = setStreamParams(movie.length, range);
        let stream = movie.createReadStream({
            start: streamParams.start,
            end: streamParams.end
        });
        res = writeHeader(res, 206, streamParams);
        if (toConvert) {
            new Transcoder(stream).format("webm").stream().pipe(res);
        } else {
            stream.pipe(res);
        }
    }
}

function asyncFromTorrentToMagnet(archiveId) {
    const archiveTorrentLink = "https://archive.org/download/" + archiveId + "/" + archiveId + "_archive.torrent";
    return new Promise((resolve) => {
        torrentToMagnet(archiveTorrentLink, {}, (err, uri) => {
            resolve(uri);
        });
    })
}

async function getMagnetLink(request) {
    const ytsHash = request.query.ytsHash;
    const archiveId = request.query.archiveId;
    const ytsMagnetPrefix = "magnet:?xt=urn:btih:";
    if (ytsHash) {
        return ytsMagnetPrefix + ytsHash;
    } else {
        return await asyncFromTorrentToMagnet(archiveId)
    }
}

function getOptions() {
    return {
        tmp: "/tmp",
        path: path.normalize(__dirname + "../../../Downloads/Torrents")
    }
}

function movieIsStored(imdbId) {
    const file = fs.readdirSync(moviePath).find(file => path.basename(file, path.extname(file)) === imdbId);
    return typeof file !== 'undefined';
}

function manageTorrentDownload(req, res, magnetLink, imdbId) {
    let movie = {};
    const options = getOptions();
    const engine = torrentStream(magnetLink, options);
    let toConvert = false;
    let foundFile = false;
    engine.on("ready", () => {
        engine.files.forEach(async file => {
            if (!foundFile && (path.extname(file.name) === ".mp4" || path.extname(file.name) === ".webm" || path.extname(file.name) === '.mkv')) {
                foundFile = true;
                file.select();
                movie = file;
                if (path.extname(file.name) === '.mkv') {
                    toConvert = true;
                }
                streamDownloadingMovie(movie, req.headers.range, res, toConvert, imdbId);
            } else {
                file.deselect();
            }
        });
    });

    engine.on("download", function () {
        console.log("file Download : " + (100 * engine.swarm.downloaded) /
            movie.length + "%");
    })

    engine.on("idle", function () {
        if (!movieIsStored(imdbId)) {
            const oldPath = path.join(options.path, movie.path);
            const newPath = path.format({
                dir: moviePath,
                name: imdbId,
                ext: path.extname(movie.name)
            });
            if (toConvert) {
                new Transcoder(stream).format("webm").writeToFile(path.format({
                    dir: moviePath,
                    name: imdbId,
                    ext: ".webm"
                }));
            } else {
                fs.renameSync(oldPath, newPath);
            }
        }
    })
}

function downloadTorrent(req, res) {
    const imdbId = req.query.imdbId;
    const magnetLink = getMagnetLink(req);
    magnetLink.then(link => {
        manageTorrentDownload(req, res, link, imdbId);
    });
}

function streamFile(file, req, res) {
    const streamParams = setStreamParams(fs.statSync(file).size, req.headers.range);
    res = writeHeader(res, 206, streamParams);
    fs.createReadStream(file, {start: streamParams.start, end: streamParams.end}).pipe(res);
}

function stream(req, res) {
    const imdbId = req.query.imdbId;
    fs.readdir(moviePath, function (err, files) {
        if (err) {
            res.status(500).send("Error fs: could not read directory");
        }
        const movie = files.find(file => path.basename(file, path.extname(file)) === imdbId);
        if (movie) {
            streamFile(path.join(moviePath, movie), req, res);
        } else {
            downloadTorrent(req, res);
        }
    });
}

module.exports = {stream};
