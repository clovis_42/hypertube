const OS = require('opensubtitles-api');
const openSubtitles = new OS({
    useragent: 'UserAgent',
    username: '', // TODO fill the username parameter with your own
    password: '', // TODO fill the password parameter with your own password
    ssl: true
});
let apiToken = null;

openSubtitles.login()
    .then(res => {
        apiToken = res.token;
    })
    .catch(err => {
        console.log(err);
    });


function setQuery(imdbId) {
    const lang = ['eng', 'fre'];
    return {
        sublanguageid: lang.join(),
        extensions: ['srt', 'vtt'],
        imdbid: imdbId
    };
}

function formatSubtitlesResponse(subtitles) {
    const objSubtitles = {
        fr: '',
        en: ''
    };
    objSubtitles.fr = subtitles.fr.vtt;
    objSubtitles.en = subtitles.en.vtt;
    return objSubtitles;
}

async function searchSubtitles(query) {
    let response = {
        body: '',
        status: 0
    }
    return await openSubtitles.search(query)
        .then(subtitles => {
            response.body = formatSubtitlesResponse(subtitles);
            response.status = 200;
            return response;
        })
        .catch(err => {
            response.status = 404;
            response.body = "Error fetching subtitles: " + err;
            return response;
        });
}

function getSubtitles(req, res) {
    const imdbId = req.query.imdbId;
    const query = setQuery(imdbId);
    const subtitles = searchSubtitles(query, res);
    subtitles.then(response => {
        res.status(response.status).send(response.body);
    });
}

module.exports = {getSubtitles};
