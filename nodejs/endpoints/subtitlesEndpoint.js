const express = require('express');
const cors = require('cors');
const router = express.Router();
const subtitlesHandler = require('../handlers/subtitlesHandler');

router.get('/get', cors(), function (req, res) {
    subtitlesHandler.getSubtitles(req, res)
})

module.exports = router;
