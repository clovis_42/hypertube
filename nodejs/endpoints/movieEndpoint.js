const express = require('express');
const router = express.Router();
const moviesHandler = require('../handlers/moviesHandler');

router.get('/stream', function (req, res) {
    moviesHandler.stream(req, res)
})

module.exports = router;
