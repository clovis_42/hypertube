const express = require('express');
const router = express.Router()

router.use('/movie', require('./movieEndpoint'));
router.use('/subtitles', require('./subtitlesEndpoint'));

module.exports = router;
