const express = require('express');
const app = express();
const port = 3000;
const router = require('./endpoints/endpoints');
const cronHandler = require('./handlers/CronHandler');

app.use('/', router);
cronHandler.dailyCleaner();

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});
